-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2019 at 06:40 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stokis`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_in`
--

CREATE TABLE `detail_in` (
  `id` int(11) NOT NULL,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `kd_produk` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detail_in`
--

INSERT INTO `detail_in` (`id`, `nomor_faktur`, `kd_produk`, `qty`, `pembuat`) VALUES
(8, 'P0001', 'B0001', 20, 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `detail_out`
--

CREATE TABLE `detail_out` (
  `id` int(11) NOT NULL,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `kd_produk` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `head_office`
--

CREATE TABLE `head_office` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `situs` varchar(50) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `akses_system` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nik`, `nama_lengkap`, `alamat`, `tempat_lahir`, `tgl_lahir`, `email`, `telp`, `jenis_kelamin`, `akses_system`, `aktiv`, `tanggal_input`, `pembuat`) VALUES
(1, 'administrator', 'Administrator', 'Jakarta', 'Lombok', '1987-04-02', 'admin@gmail.com', '', 'Laki-laki', 1, 1, '2019-04-23 16:50:35', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `ktp` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_join` varchar(50) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `label` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `posisi` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `label`, `path`, `icon`, `posisi`, `aktiv`, `tanggal_input`, `pembuat`) VALUES
(1, 0, 'Dashboard', 'dashboard', 'fa fa-dashboard', 1, 1, '2019-04-23 16:50:35', 1),
(2, 0, 'Data Master', '#', 'fa fa-desktop', 2, 1, '2019-04-23 16:50:35', 1),
(3, 0, 'Transaksi', '#', 'fa fa-edit', 3, 1, '2019-04-23 16:50:35', 1),
(4, 0, 'Laporan', '#', 'fa fa-pie-chart', 4, 1, '2019-04-23 16:50:35', 1),
(5, 2, 'Product', 'master-product', 'fa fa-angle-double-right', 1, 1, '2019-04-23 16:50:35', 1),
(6, 2, 'Member', 'master-member', 'fa fa-angle-double-right', 2, 1, '2019-04-23 16:50:35', 1),
(7, 2, 'Karyawan', 'master-karyawan', 'fa fa-angle-double-right', 3, 1, '2019-04-23 16:50:35', 1),
(8, 2, 'User', 'master-user', 'fa fa-angle-double-right', 4, 1, '2019-04-23 16:50:35', 1),
(9, 2, 'Head Office', 'master-headoffice', 'fa fa-angle-double-right', 5, 1, '2019-04-23 16:50:35', 1),
(10, 2, 'Stokis', 'master-stokis', 'fa fa-angle-double-right', 6, 1, '2019-04-23 16:50:35', 1),
(11, 3, 'Product Masuk', 'transaksi-product-masuk', 'fa fa-angle-double-right', 1, 1, '2019-04-23 16:50:35', 1),
(12, 3, 'Product Keluar', 'transaksi-product-keluar', 'fa fa-angle-double-right', 2, 1, '2019-04-23 16:50:35', 1),
(13, 4, 'Stock Product', 'laporan-stock-product', 'fa fa-angle-double-right', 1, 1, '2019-04-23 16:50:35', 1),
(14, 4, 'History Product', 'laporan-history-product', 'fa fa-angle-double-right', 2, 1, '2019-04-23 16:50:35', 1),
(15, 4, 'Product Masuk', 'laporan-product-masuk', 'fa fa-angle-double-right', 3, 1, '2019-04-23 16:50:35', 1),
(16, 4, 'Product Keluar', 'laporan-product-keluar', 'fa fa-angle-double-right', 4, 1, '2019-04-23 16:50:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `kd_produk` varchar(10) DEFAULT NULL,
  `nama_produk` varchar(50) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `kd_produk`, `nama_produk`, `aktiv`, `tanggal_input`, `pembuat`) VALUES
(10, 'B0001', 'Mozarela', 1, '2019-08-10 06:30:28', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`, `deskripsi`, `aktiv`, `tanggal_input`, `pembuat`) VALUES
(1, 'administrator', 'Administrator', 1, '2019-04-23 16:50:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`, `aktiv`, `tanggal_input`, `pembuat`) VALUES
(1, 1, 1, 1, '2019-04-23 16:50:35', '1'),
(2, 1, 2, 1, '2019-04-23 16:50:35', '1'),
(3, 1, 3, 1, '2019-04-23 16:50:35', '1'),
(4, 1, 4, 1, '2019-04-23 16:50:35', '1'),
(5, 1, 5, 1, '2019-04-23 16:50:35', '1'),
(6, 1, 6, 1, '2019-04-23 16:50:35', '1'),
(7, 1, 7, 1, '2019-04-23 16:50:35', '1'),
(8, 1, 8, 1, '2019-04-23 16:50:35', '1'),
(9, 1, 9, 1, '2019-04-23 16:50:35', '1'),
(10, 1, 10, 1, '2019-04-23 16:50:35', '1'),
(11, 1, 11, 1, '2019-04-23 16:50:35', '1'),
(12, 1, 12, 1, '2019-04-23 16:50:35', '1'),
(13, 1, 13, 1, '2019-04-23 16:50:35', '1'),
(14, 1, 14, 1, '2019-04-23 16:50:35', '1'),
(15, 1, 15, 1, '2019-04-23 16:50:35', '1'),
(16, 1, 16, 1, '2019-04-23 16:50:35', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sequence`
--

CREATE TABLE `sequence` (
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sequence`
--

INSERT INTO `sequence` (`no`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kd_product` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kd_product`, `qty`) VALUES
(7, 'B0001', 20);

-- --------------------------------------------------------

--
-- Table structure for table `stock_history`
--

CREATE TABLE `stock_history` (
  `id` int(11) NOT NULL,
  `kd_product` varchar(10) DEFAULT NULL,
  `qty_in` int(11) DEFAULT NULL,
  `qty_out` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_history`
--

INSERT INTO `stock_history` (`id`, `kd_product`, `qty_in`, `qty_out`, `tanggal_input`, `pembuat`) VALUES
(11, 'B0001', 20, 0, '2019-08-10 06:36:23', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `stokis`
--

CREATE TABLE `stokis` (
  `id` int(11) NOT NULL,
  `nama_stokis` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_in`
--

CREATE TABLE `transaksi_in` (
  `id` int(11) NOT NULL,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `jml_product` int(11) DEFAULT NULL,
  `pengirim` int(11) DEFAULT NULL,
  `tgl_masuk` varchar(50) DEFAULT NULL,
  `penerima` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaksi_in`
--

INSERT INTO `transaksi_in` (`id`, `nomor_faktur`, `jml_product`, `pengirim`, `tgl_masuk`, `penerima`, `tanggal_input`, `pembuat`) VALUES
(3, 'P0001', 1, NULL, '2019-08-10', NULL, '2019-08-10 06:36:23', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_out`
--

CREATE TABLE `transaksi_out` (
  `id` int(11) NOT NULL,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `jml_produk` int(11) DEFAULT NULL,
  `pengirim` int(11) DEFAULT NULL,
  `tgl_kirim` varchar(50) DEFAULT NULL,
  `penerima` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nik`, `password`, `role_id`, `aktiv`, `tanggal_input`, `pembuat`) VALUES
(1, 'administrator', '4297f44b13955235245b2497399d7a93', 1, 1, '2019-04-23 16:50:35', 'administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_in`
--
ALTER TABLE `detail_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_out`
--
ALTER TABLE `detail_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `head_office`
--
ALTER TABLE `head_office`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sequence`
--
ALTER TABLE `sequence`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_history`
--
ALTER TABLE `stock_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stokis`
--
ALTER TABLE `stokis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_in`
--
ALTER TABLE `transaksi_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_out`
--
ALTER TABLE `transaksi_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_in`
--
ALTER TABLE `detail_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `detail_out`
--
ALTER TABLE `detail_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `head_office`
--
ALTER TABLE `head_office`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `stock_history`
--
ALTER TABLE `stock_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `stokis`
--
ALTER TABLE `stokis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaksi_in`
--
ALTER TABLE `transaksi_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaksi_out`
--
ALTER TABLE `transaksi_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
