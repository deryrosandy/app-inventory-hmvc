/*
Navicat MySQL Data Transfer

Source Server         : dms
Source Server Version : 100136
Source Host           : localhost:3306
Source Database       : stokis

Target Server Type    : MYSQL
Target Server Version : 100136
File Encoding         : 65001

Date: 2019-05-06 07:57:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for detail_in
-- ----------------------------
DROP TABLE IF EXISTS `detail_in`;
CREATE TABLE `detail_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `kd_produk` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of detail_in
-- ----------------------------

-- ----------------------------
-- Table structure for detail_out
-- ----------------------------
DROP TABLE IF EXISTS `detail_out`;
CREATE TABLE `detail_out` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `kd_produk` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of detail_out
-- ----------------------------

-- ----------------------------
-- Table structure for head_office
-- ----------------------------
DROP TABLE IF EXISTS `head_office`;
CREATE TABLE `head_office` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `situs` varchar(50) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of head_office
-- ----------------------------

-- ----------------------------
-- Table structure for karyawan
-- ----------------------------
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `akses_system` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of karyawan
-- ----------------------------
INSERT INTO `karyawan` VALUES ('1', 'administrator', 'Administrator', 'Jakarta', 'Bekasi', '1994-03-25', 'admin@gmail.com', '', 'Laki-laki', '1', '1', '2019-04-23 16:50:35', 'administrator');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ktp` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_join` varchar(50) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `label` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `posisi` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '0', 'Dashboard', 'dashboard', 'fa fa-dashboard', '1', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('2', '0', 'Data Master', '#', 'fa fa-desktop', '2', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('3', '0', 'Transaksi', '#', 'fa fa-edit', '3', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('4', '0', 'Laporan', '#', 'fa fa-pie-chart', '4', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('5', '2', 'Product', 'master-product', 'fa fa-angle-double-right', '1', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('6', '2', 'Member', 'master-member', 'fa fa-angle-double-right', '2', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('7', '2', 'Karyawan', 'master-karyawan', 'fa fa-angle-double-right', '3', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('8', '2', 'User', 'master-user', 'fa fa-angle-double-right', '4', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('9', '2', 'Head Office', 'master-headoffice', 'fa fa-angle-double-right', '5', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('10', '2', 'Stokis', 'master-stokis', 'fa fa-angle-double-right', '6', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('11', '3', 'Product Masuk', 'transaksi-product-masuk', 'fa fa-angle-double-right', '1', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('12', '3', 'Product Keluar', 'transaksi-product-keluar', 'fa fa-angle-double-right', '2', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('13', '4', 'Stock Product', 'laporan-stock-product', 'fa fa-angle-double-right', '1', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('14', '4', 'History Product', 'laporan-history-product', 'fa fa-angle-double-right', '2', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('15', '4', 'Product Masuk', 'laporan-product-masuk', 'fa fa-angle-double-right', '3', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `menu` VALUES ('16', '4', 'Product Keluar', 'laporan-product-keluar', 'fa fa-angle-double-right', '4', '1', '2019-04-23 16:50:35', '1');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kd_produk` varchar(10) DEFAULT NULL,
  `nama_produk` varchar(50) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'administrator', 'Administrator', '1', '2019-04-23 16:50:35', '1');

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('1', '1', '1', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('2', '1', '2', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('3', '1', '3', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('4', '1', '4', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('5', '1', '5', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('6', '1', '6', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('7', '1', '7', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('8', '1', '8', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('9', '1', '9', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('10', '1', '10', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('11', '1', '11', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('12', '1', '12', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('13', '1', '13', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('14', '1', '14', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('15', '1', '15', '1', '2019-04-23 16:50:35', '1');
INSERT INTO `role_menu` VALUES ('16', '1', '16', '1', '2019-04-23 16:50:35', '1');

-- ----------------------------
-- Table structure for sequence
-- ----------------------------
DROP TABLE IF EXISTS `sequence`;
CREATE TABLE `sequence` (
  `no` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sequence
-- ----------------------------
INSERT INTO `sequence` VALUES ('1');

-- ----------------------------
-- Table structure for stock
-- ----------------------------
DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kd_product` varchar(10) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock
-- ----------------------------

-- ----------------------------
-- Table structure for stock_history
-- ----------------------------
DROP TABLE IF EXISTS `stock_history`;
CREATE TABLE `stock_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kd_product` varchar(10) DEFAULT NULL,
  `qty_in` int(11) DEFAULT NULL,
  `qty_out` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_history
-- ----------------------------

-- ----------------------------
-- Table structure for stokis
-- ----------------------------
DROP TABLE IF EXISTS `stokis`;
CREATE TABLE `stokis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_stokis` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stokis
-- ----------------------------

-- ----------------------------
-- Table structure for transaksi_in
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_in`;
CREATE TABLE `transaksi_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `jml_product` int(11) DEFAULT NULL,
  `pengirim` int(11) DEFAULT NULL,
  `tgl_masuk` varchar(50) DEFAULT NULL,
  `penerima` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transaksi_in
-- ----------------------------

-- ----------------------------
-- Table structure for transaksi_out
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_out`;
CREATE TABLE `transaksi_out` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_faktur` varchar(50) DEFAULT NULL,
  `jml_produk` int(11) DEFAULT NULL,
  `pengirim` int(11) DEFAULT NULL,
  `tgl_kirim` varchar(50) DEFAULT NULL,
  `penerima` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transaksi_out
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `aktiv` int(11) DEFAULT NULL,
  `tanggal_input` varchar(50) DEFAULT NULL,
  `pembuat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'administrator', '4297f44b13955235245b2497399d7a93', '1', '1', '2019-04-23 16:50:35', 'administrator');
SET FOREIGN_KEY_CHECKS=1;
