  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url()?>asset/images/lau.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('fullname') ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php
            function createTree($menu, $level = 0)
            {
              if(count($menu) > 0)
              {
                if($level == 0)
                {
                    $strUlClass     = 'sidebar-menu';
                    $param          = 'data-widget="tree"';
                    $base_url       = "<?php echo site_url("."'";
                    $end_url        = "'".")?>";

                    echo '<ul class="'.$strUlClass.'" '.$param.'>';
                    echo '<li class="header">MAIN NAVIGATION</li>';
                }
                else
                {
                    $strUlClass     = 'treeview-menu';
                    $param          = '';

                    echo '<ul class="'.$strUlClass.'">';
                }

                $font     = 'style=';
                $satu     = '"font-family:';
                $dua      = "'verdana'";
                $tiga     = '"';
                $fontmenu = $font.$satu.$dua.$tiga;

                foreach($menu as $objItem)
                {  
                  if(isset($objItem->arrChilds) && count($objItem->arrChilds) > 0)
                  {
                    echo '<li class="treeview" '.$fontmenu.'><a href="'.base_url().$objItem->path.'"><i class="fa '.$objItem->icon.'"></i><span>'.$objItem->label.'</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>'; 
                    createTree($objItem->arrChilds,$level+1);
                  }
                  else
                  {
                    echo '<li '.$fontmenu.'><a href="'.base_url().$objItem->path.'"><i class="fa '.$objItem->icon.'"></i><span>'.$objItem->label.'</span></a>';
                  }
                  echo '</li>';
                }
                echo '</ul>';
              }
              else
              {
                echo '<li class="header" style="color#fff;"></li>';
              }
            }
            createTree($menu);
        ?>
    </section>
    <!-- /.sidebar -->
  </aside>