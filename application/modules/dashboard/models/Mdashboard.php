<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdashboard extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Jmlproduct()
	{
		try
		{
			$sql	= "select count(id) as id from product";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmlmember()
	{
		try
		{
			$sql	= "select count(id) as id from member";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmlkaryawan()
	{
		try
		{
			$sql	= "select count(id) as id from karyawan";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmluser()
	{
		try
		{
			$sql	= "select count(id) as id from user";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmloffice()
	{
		try
		{
			$sql	= "select count(id) as id from head_office";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmlstokis()
	{
		try
		{
			$sql	= "select count(id) as id from stokis";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmlin()
	{
		try
		{
			$sql	= "select count(id) as id from transaksi_in";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function Jmlout()
	{
		try
		{
			$sql	= "select count(id) as id from transaksi_out";
			$query	= $this->db->query($sql)->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

}

/* End of file Mdashboard.php */
/* Location: ./application/modules/dashboard/models/Mdashboard.php */