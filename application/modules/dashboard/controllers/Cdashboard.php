<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdashboard extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mdashboard');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['product']	= $this->Mdashboard->Jmlproduct();
		$data['member']		= $this->Mdashboard->Jmlmember();
		$data['karyawan']	= $this->Mdashboard->Jmlkaryawan();
		$data['user']		= $this->Mdashboard->Jmluser();
		$data['office']		= $this->Mdashboard->Jmloffice();
		$data['stokis']		= $this->Mdashboard->Jmlstokis();
		$data['in']			= $this->Mdashboard->Jmlin();
		$data['out']		= $this->Mdashboard->Jmlout();
		$data['menu']		= $this->Mdashboard->getMenu($role_id);
		$data['content']	= "Vdashboard";
		$this->load->view('layout/template',$data);
	}

}

/* End of file Cdashboard.php */
/* Location: ./application/modules/dashboard/controllers/Cdashboard.php */