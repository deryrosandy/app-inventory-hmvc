<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
        <h4 style="font-family: 'Timew New Rowman'"><b>Profile</b></h4>    
		<hr/>
		<div class="callout callout-info">
		<div class="row">
			<div class="col-lg-4">
				<table>
					<tr>
						<td><b>Nik</b></td>
						<td><b>&nbsp;:</b></td>
						<td><?php echo $this->session->userdata('nik')?></td>
					</tr>
					<tr>
						<td><b>Nama Lengkap</b></td>
						<td><b>&nbsp;:</b></td>
						<td><?php echo $this->session->userdata('nama_lengkap')?></td>
					</tr>
					<tr>
						<td><b>Tempat, Tgl Lahir</b></td>
						<td><b>&nbsp;:</b></td>
						<td><?php echo $this->session->userdata('tempat_lahir')?>, <?php echo $this->session->userdata('tgl_lahir')?></td>
					</tr>
					<tr>
						<td><b>Alamat</b></td>
						<td><b>&nbsp;:</b></td>
						<td><?php echo $this->session->userdata('alamat')?></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-4">
				<table>
					<tr>
						<td><b>Email</b></td>
						<td><b>&nbsp;:</b></td>
						<td><?php echo $this->session->userdata('email')?></td>
					</tr>
					<tr>
						<td><b>No Telp</b></td>
						<td><b>&nbsp;:</b></td>
						<td><?php echo $this->session->userdata('telp')?></td>
					</tr>
				</table>
			</div>
		</div>
		</div>
		<h4 style="font-family: 'Timew New Rowman'"><b>Data Master</b></h4>
		<hr/>
		<div class="row">
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-aqua">
	            <div class="inner">
	              <h3><?php echo $product ?></h3>
	              <p>Product</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-database"></i>
	            </div>
	            <a href="<?php echo site_url('master-product')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-green">
	            <div class="inner">
	              <h3><?php echo $member ?></h3>

	              <p>Supplier</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-user"></i>
	            </div>
	            <a href="<?php echo site_url('master-member')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-yellow">
	            <div class="inner">
	              <h3><?php echo $karyawan ?></h3>

	              <p>Karyawan</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-users"></i>
	            </div>
	            <a href="<?php echo site_url('master-karyawan')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-yellow">
	            <div class="inner">
	              <h3><?php echo $user ?></h3>

	              <p>User</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-users"></i>
	            </div>
	            <a href="<?php echo site_url('master-user')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
      	</div>
      	<div class="row">
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner">
	              <h3><?php echo $office ?></h3>
	              <p>Head Office</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-database"></i>
	            </div>
	            <a href="<?php echo site_url('master-headoffice')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner">
	              <h3><?php echo $stokis ?></h3>

	              <p>Stokis</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-database"></i>
	            </div>
	            <a href="<?php echo site_url('master-stokis')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
      	</div>
		<h4 style="font-family: 'Timew New Rowman'"><b>Transaksi</b></h4>
		<hr/>
		<div class="row">
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-aqua">
	            <div class="inner">
	              <h3><?php echo $in ?></h3>
	              <p>Product Masuk</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-edit"></i>
	            </div>
	            <a href="<?php echo site_url('transaksi-product-masuk')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner">
	              <h3><?php echo $out ?></h3>

	              <p>Product Keluar</p>
	            </div>
	            <div class="icon">
	              <i class="fa fa-fw fa-edit"></i>
	            </div>
	            <a href="<?php echo site_url('transaksi-product-keluar')?>" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
	        </div>
      	</div>
	</div>
</div>