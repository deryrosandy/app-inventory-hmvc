<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mprofile extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Profile($nik)
	{
		try
		{
			$sql	= "select * from karyawan where nik = ?";
			$query	= $this->db->query($sql,array($nik))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function simpan_update_profile($nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp)
	{
		try
		{
			$sql	= "update karyawan set nama_lengkap = ?, alamat = ?, tempat_lahir = ?, tgl_lahir = ?,
					   email = ?, telp = ? where nik = ?";
			$query	= $this->db->query($sql,array($nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,$nik));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}		
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_update_password($nik,$password)
	{
		try
		{
			$sql	= "update user set password = ? where nik = ?";
			$query	= $this->db->query($sql,array($password,$nik));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}		
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

}

/* End of file Mprofile.php */
/* Location: ./application/modules/profile/models/Mprofile.php */