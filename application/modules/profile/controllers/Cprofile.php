<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cprofile extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mprofile');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mprofile->getMenu($role_id);
		$nik				= $this->session->userdata('nik');
		$data['profile']	= $this->Mprofile->Profile($nik);
		$data['content']	= "Vprofile";
		$this->load->view('layout/template',$data);
	}

	public function simpan_update_profile()
	{
		$nik		= $this->input->post('nik');
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$tmp_lahir	= str_replace("'","",$this->input->post('tmp_lahir'));
		$tgl_lahir 	= date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$telp 		= $this->input->post('telp');
		$simpan 	= $this->Mprofile->simpan_update_profile($nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp);
		if($simpan == TRUE)
		{
			$this->session->set_flashdata('erorr_message','Data Profile Berhasil di Update');
			redirect('logout');
		}
	}

	public function password()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mprofile->getMenu($role_id);
		$data['content']	= "Vpassword";
		$this->load->view('layout/template',$data);
	}

	public function simpan_update_password()
	{
		$nik		= $this->session->userdata('nik');
		$password 	= md5($this->input->post('password').'123');
		$update 	= $this->Mprofile->simpan_update_password($nik,$password);
		if($update == TRUE)
		{
			$this->session->set_flashdata('erorr_message','Password Berhasil di Update');
			redirect('logout');
		}
	}

}

/* End of file Cprofile.php */
/* Location: ./application/modules/profile/controllers/Cprofile.php */