<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Update Password</b></h4>
		<br/>
		<form action="<?php echo site_url('simpan-update-password')?>" method="post">
			<div class="row">
	        	<div class="col-lg-6">
	        		<div class="form-group">
		                <label for="exampleInputEmail1">Password Baru</label>
		                <input type="password" class="form-control" name="password" id="password">
		            </div>
	        	</div>
	        </div>
			<a href="<?php echo site_url('dashboard')?>">
	        	<button type="button" class="btn btn-danger">Batal</button>
	        </a>
          	<button type="submit" class="btn btn-primary">Update</button>
		</form>
	</div>
</div>