<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Update Profile</b></h4>
		<br/>
		<form action="<?php echo site_url('simpan-update-profile')?>" method="post">
			<?php foreach($profile as $data){?>
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
		                <label for="exampleInputEmail1">Nik</label>
		                <input type="text" class="form-control" name="nik" id="nik" required="" readonly="" value="<?php echo $data['nik']?>">
		            </div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
		                <label for="exampleInputEmail1">Nama Lengkap</label>
		                <input type="text" class="form-control" name="nama" id="nama" required="" value="<?php echo $data['nama_lengkap']?>">
		            </div>
				</div>
			</div>
			<div class="form-group">
	            <label for="exampleInputEmail1">Alamat</label>
	            <textarea class="form-control" name="alamat" id="alamat"><?php echo $data['alamat']?></textarea>
	        </div>
	        <div class="row">
	        	<div class="col-lg-3">
	        		<div class="form-group">
		                <label for="exampleInputEmail1">Tempat Lahir</label>
		                <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" value="<?php echo $data['tempat_lahir']?>">
		            </div>
	        	</div>
	        	<div class="col-lg-3">
	        		<div class="form-group">
		                <label for="exampleInputEmail1">Tanggal Lahir</label>
		                <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" value="<?php echo $data['tgl_lahir']?>">
		            </div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-lg-3">
	        		<div class="form-group">
		                <label for="exampleInputEmail1">Email</label>
		                <input type="email" class="form-control" name="email" id="email" style="text-transform: lowercase;" value="<?php echo $data['email']?>">
		            </div>
	        	</div>
	        	<div class="col-lg-3">
	        		<div class="form-group">
		                <label for="exampleInputEmail1">Telp</label>
		                <input type="text" class="form-control" name="telp" id="telp" value="<?php echo $data['telp']?>">
		            </div>
	        	</div>
	        </div>
	    	<?php } ?>
	        <a href="<?php echo site_url('dashboard')?>">
	        	<button type="button" class="btn btn-danger">Batal</button>
	        </a>
          	<button type="submit" class="btn btn-primary">Update</button>
		</form>
	</div>
</div>