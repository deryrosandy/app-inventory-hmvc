<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproduct_keluar extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Member()
	{
		try
		{
			$sql	= "select id, nama from member where aktiv = 1 order by nama";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Stokis()
	{
		try
		{
			$sql	= "select distinct id, nama_stokis, alamat from stokis";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Product()
	{
		try
		{
			$sql	= "select id, kd_produk, nama_produk from product where aktiv = 1 order by kd_produk";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Detail($nik)
	{
		try
		{
			$sql	= "select a.id, a.kd_produk, b.nama_produk, a.qty from detail_out a
					   left join product b
					   on a.kd_produk = b.kd_produk
					   where (a.nomor_faktur is null or a.nomor_faktur = '')
					   and a.pembuat = ?";
			$query	= $this->db->query($sql,array($nik))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function get_stock($produk)
	{
		try
		{
			$sql	= "select qty from stock where kd_product = ?";
			$query	= $this->db->query($sql,array($produk))->result_array();

			$qty	= 0;
			foreach($query as $data)
			{
				$qty = $data['qty'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $qty;
	}

	public function tambah_detail_keluar($kd_produk,$qty,$nik)
	{
		try
		{
			$sql	= "select count(id) as id from detail_out
					   where (nomor_faktur is null or nomor_faktur = '')
					   and pembuat = ?
					   and kd_produk = ?";
			$query  = $this->db->query($sql,array($nik,$kd_produk))->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}

			if($id > 0)
			{
				$getqty	= "select qty from detail_out 
						   where (nomor_faktur is null or nomor_faktur = '')
					   	   and pembuat = ?
					       and kd_produk = ?";
				$exec1	= $this->db->query($getqty,array($nik,$kd_produk))->result_array();

				$qtys	= 0;
				foreach($exec1 as $data)
				{
					$qtys	= $data['qty'];
				}

				$fqty		= $qty + $qtys;

				$update 	= "update detail_out set qty = ?
							   where (nomor_faktur is null or nomor_faktur = '')
					   	  	   and pembuat = ?
					           and kd_produk = ?";
				$exec2 		= $this->db->query($update,array($fqty,$nik,$kd_produk));

				$stock 		= "select qty from stock where kd_product = ?";
				$exec3		= $this->db->query($stock,array($kd_produk))->result_array();

				foreach($exec3 as $data)
				{
					$xqty = $data['stock'];
				}

				$zqty	= $xqty - $qty;

				$xxx	= "update stock set qty = ? where kd_product = ?";
				$zzz	= $this->db->query($xxx,array($zqty,$kd_produk));
			}
			else
			{
				$insert 	= "insert into detail_out (kd_produk,qty,pembuat) values (?,?,?)";
				$exec2 		= $this->db->query($insert,array($kd_produk,$qty,$nik));

				$stock 		= "select qty from stock where kd_product = ?";
				$exec3		= $this->db->query($stock,array($kd_produk))->result_array();

				foreach($exec3 as $data)
				{
					$xqty = $data['qty'];
				}

				$zqty	= $xqty - $qty;

				$xxx	= "update stock set qty = ? where kd_product = ?";
				$zzz	= $this->db->query($xxx,array($zqty,$kd_produk));
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($zzz)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function hapus_detail_keluar($id_detail)
	{
		try
		{
			$detail = "select kd_produk, qty from detail_out where id = ?";
			$exec 	= $this->db->query($detail,array($id_detail))->result_array();

			foreach($exec as $data)
			{
				$kd_produk 	= $data['kd_produk'];
				$qty 		= $data['qty'];
			}

			$stock 	= "select qty from stock where kd_product = ?";
			$exec2	= $this->db->query($stock,array($kd_produk))->result_array();

			foreach($exec2 as $data)
			{
				$sqty	= $data['qty'];
			}

			$fqty	= $qty + $sqty;

			$update	= "update stock set qty = ? where kd_product = ?";
			$exec3	= $this->db->query($update,array($fqty,$kd_produk));

			$sql	= "delete from detail_out where id = ?";
			$query	= $this->db->query($sql,array($id_detail));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_master($pengirim,$penerima,$jml_produk,$tanggal_input,$pembuat)
	{
		try
		{
			$sql	= "select no from sequence";
			$exec 	= $this->db->query($sql)->result_array();

			foreach($exec as $data)
			{
				$no = $data['no'];
			}

			$next	= $no + 1;

			$update	= "update sequence set no = ?";
			$exec2	= $this->db->query($update,array($next));

			$no_faktur = "FTR/".str_replace("-","",date('Y-m-d'))."/".$no;

			$sql	= "insert into transaksi_out (nomor_faktur,jml_produk,pengirim,tgl_kirim,penerima,tanggal_input,pembuat) 
					   values (?,?,?,?,?,?,?)";
			$query	= $this->db->query($sql,array($no_faktur,$jml_produk,$pengirim,$tanggal_input,$penerima,$tanggal_input,$pembuat));

			$update	= "update detail_out set nomor_faktur = ?
					   where nomor_faktur is null
					   and pembuat = ?";
			$exec3 	= $this->db->query($update,array($no_faktur,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec3)
		{
			$this->db->trans_commit();
			return $no_faktur;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_detail($kd_produk,$qty,$tanggal_input,$pembuat)
	{
		try
		{
			
			$history	= "insert into stock_history (kd_product,qty_in,qty_out,tanggal_input,pembuat) values (?,?,?,?,?)";
			$exec3		= $this->db->query($history,array($kd_produk,0,$qty,$tanggal_input,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec3)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Fakturkeluar()
	{
		try
		{
			$sql	= "select a.id, a.nomor_faktur, a.jml_produk, substr(a.tgl_kirim,1,10) as tgl_kirim, a.tanggal_input, b.nama_lengkap,
					   c.nama, d.nama_stokis
					   from transaksi_out a
					   left join karyawan b
					   on a.pembuat = b.nik
					   left join head_office c
					   on a.pengirim = c.id
					   left join stokis d
					   on a.penerima = d.id
					   order by a.id";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;		
	}

	public function Masterprint($faktur_id)
	{
		try
		{
			$sql	= "select a.id, a.nomor_faktur, b.nama_stokis as nama, a.tgl_kirim, a.tanggal_input,
					   c.alamat, c.telp, c.email, b.alamat as head_alamat, b.telp as head_telp,
					   b.email as head_email, c.nama as member
					   from transaksi_out a
					   left join stokis b
					   on a.pengirim = b.id
					   left join member c
					   on a.penerima = c.id
					   where a.id = ?";
			$query	= $this->db->query($sql,array($faktur_id))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Detailprint($faktur_id)
	{
		try
		{
			$sql	= "select nomor_faktur from transaksi_out where id = ?";
			$query	= $this->db->query($sql,array($faktur_id))->result_array();

			foreach($query as $data)
			{
				$nomor_faktur = $data['nomor_faktur'];
			}

			$detail = "select a.kd_produk, a.qty, b.nama_produk from detail_out a
					   left join product b
					   on a.kd_produk = b.kd_produk
					   where a.nomor_faktur = ?"; 
			$exec 	= $this->db->query($detail,array($nomor_faktur))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $exec;
	}

}

/* End of file Mproduct_keluar.php */
/* Location: ./application/modules/product_keluar/models/Mproduct_keluar.php */