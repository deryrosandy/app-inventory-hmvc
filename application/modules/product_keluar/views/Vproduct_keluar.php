<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweetalert/sweetalert.css">
<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>

<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Data Faktur Keluar</b></h4>
		<div>
			<a href="<?php echo site_url('tambah-faktur-keluar')?>">
			<button type="button" class="btn btn-success">
        	<i class="fa fa-fw fa-edit"></i> Input Faktur Keluar</button>
        	</a>
		</div>
		<br/>
		<table id="example1" class="table table-bordered table-striped">
			<thead>
        <tr>
          <th style="text-align: center;">No</th>
          <th style="text-align: center;">Jumlah Product</th>
          <th>Nomor Faktur</th>
          <th>Pengirim</th>
          <th>Penerima</th>
          <th>Tanggal Masuk</th>
          <th>Tanggal Input</th>
          <th>Dibuat Oleh</th>
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php $n=1;
        foreach($faktur as $data){?>
        <tr>
          <td style="text-align: center;"><?php echo $n ?></td>
          <td style="text-align: center;"><?php echo $data['jml_produk']?></td>
          <td><?php echo $data['nomor_faktur']?></td>
          <td><?php echo $data['nama']?></td>
          <td><?php echo $data['nama_stokis']?></td>
          <td><?php echo $data['tgl_kirim']?></td>
          <td><?php echo $data['tanggal_input']?></td>
          <td><?php echo $data['nama_lengkap']?></td>
          <td style="text-align: center;">
            <?php $id = base64_encode($data['id'])?>
            <a href="<?php echo site_url('print-faktur-keluar/'.$id)?>" target="_blank">
              <i class="fa fa-fw fa-print"></i>
            </a>
          </td>
        </tr>
        <?php $n++; } ?>
      </tbody>
		</table>
	</div>
</div>

<script src="<?php echo base_url()?>assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/sweetalert/sweet-alerts.js" type="text/javascript"></script>
<?php if ($this->session->flashdata('success_message')): ?>
  <script>
      swal("Success", "<?php echo $this->session->flashdata('success_message')?>", "success");
  </script>
<?php endif; ?>