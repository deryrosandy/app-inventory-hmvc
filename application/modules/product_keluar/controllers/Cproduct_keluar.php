<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cproduct_keluar extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mproduct_keluar');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mproduct_keluar->getMenu($role_id);
		$data['faktur']		= $this->Mproduct_keluar->Fakturkeluar();
		$data['content']	= "Vproduct_keluar";
		$this->load->view('layout/template',$data);	
	}

	public function tambah_faktur_keluar()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mproduct_keluar->getMenu($role_id);
		$data['stokis']		= $this->Mproduct_keluar->Stokis();
		$data['member']		= $this->Mproduct_keluar->Member();
		$data['product']	= $this->Mproduct_keluar->Product();
		$nik				= $this->session->userdata('nik');
		$data['detail']		= $this->Mproduct_keluar->Detail($nik);
		$data['content']	= "Vtambah_faktur_keluar";
		$this->load->view('layout/template',$data);
	}

	public function get_stock()
	{
		$produk 		= $this->input->post('param');
		$json['qty'] 	= $this->Mproduct_keluar->get_stock($produk);
		echo json_encode($json);
	}

	public function tambah_detail_keluar()
	{
		$kd_produk	= $this->input->post('param1');
		$qty		= $this->input->post('param2');
		$nik		= $this->session->userdata('nik');
		$tambah 	= $this->Mproduct_keluar->tambah_detail_keluar($kd_produk,$qty,$nik);
		if($tambah == TRUE)
		{
			$json['status']	= 1;
			echo json_encode($json);
		}
	}

	public function hapus_detail_keluar()
	{
		$id_detail	= $this->input->post('param');
		$hapus 		= $this->Mproduct_keluar->hapus_detail_keluar($id_detail);
		if($hapus == TRUE)
		{
			$json['status']	= 1;
			echo json_encode($json);
		}
	}

	public function simpan_faktur_keluar()
	{
		$pengirim		= $this->input->post('pengirim');
		$penerima		= $this->input->post('penerima');
		$kd_produk 		= $this->input->post('kd_produk');
		$qty			= $this->input->post('qty');
		$jml_produk		= count($kd_produk);
		$tanggal_input	= date('Y-m-d H:i:s');
		$pembuat		= $this->session->userdata('nik');
		$master 		= $this->Mproduct_keluar->simpan_master($pengirim,$penerima,$jml_produk,$tanggal_input,$pembuat);
		if(!empty($master))
		{
			for($i = 0; $i < $jml_produk; $i++)
			{
				$detail = $this->Mproduct_keluar->simpan_detail($kd_produk[$i],$qty[$i],$tanggal_input,$pembuat);
			}
			if($detail == TRUE)
			{
				$this->session->set_flashdata('success_message','Data Faktur Berhasil Disimpan');
				redirect('transaksi-product-keluar');
			}
		}
	}

	public function print_faktur_keluar($id)
	{
		$faktur_id		= base64_decode($id);
		$data['master']	= $this->Mproduct_keluar->Masterprint($faktur_id);
		$data['detail']	= $this->Mproduct_keluar->Detailprint($faktur_id);
		$this->load->view('Vprint_faktur_keluar',$data);
	}

}

/* End of file Cproduct_keluar.php */
/* Location: ./application/modules/product_keluar/controllers/Cproduct_keluar.php */