<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweetalert/sweetalert.css">
<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Data Supplier</b></h4>
		<div>
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
        <i class="fa fa-fw fa-edit"></i> Tambah Supplier</button>
		</div>
		<br/>
    <form action="<?php echo site_url('update-status-member')?>" method="post">
		<table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">&#10004;</th>
            <th>No ID Supplier</th>
            <th>Nama Supplier</th>
            <th>Alamat</th>
            <th>Telp</th>
            <th style="text-align: center;">Status</th>
            <th>Tanggal Input</th>
            <th>Dibuat Oleh</th>
            <th style="text-align: center;">Aksi</th>
          </tr>
      </thead>
      <tbody>
          <?php $n=1;
          foreach($member as $data){?>
          <tr>
            <td style="text-align: center;"><?php echo $n ?></td>
            <td style="text-align: center;">
              <input type="checkbox" name="id_member[]" id="id_member_<?php echo $n ?>" value="<?php echo $data['id']?>">
            </td>
            <td><?php echo $data['ktp']?></td>
            <td><?php echo $data['nama']?></td>
            <td><?php echo $data['alamat']?></td>
            <td><?php echo $data['telp']?></td>
            <td style="text-align: center;">
              <?php if($data['aktiv'] == 1){?>
                <small class="label label-success">Aktiv</small>
              <?php }else{?>
                <small class="label label-danger">Tidak Aktiv</small>
              <?php } ?>
            </td>
            <td><?php echo $data['tanggal_input']?></td>
            <td><?php echo $data['nama_lengkap']?></td>
            <td style="text-align: center;">
              <?php $id = base64_encode($data['id'])?>
              <a href="#" title="Edit Member" data-toggle="modal" data-target="#edit-member" onclick="editmember(<?php echo $n ?>)">
                <i class="fa fa-fw fa-edit"></i>
              </a>
              <a href="<?php echo site_url('hapus-member/'.$id)?>" title="Hapus Member" onclick="return confirm('Yakin di Hapus ?')">
                <i class="fa fa-fw fa-close" style="color: red"></i>
              </a>
            </td>
          </tr>
          <?php $n++; } ?>
      </tbody>
    </table>
    <p style="color: red">Note : Centang sebelum melakukan perubahan status Supplier</p>
    <div class="row">
    	<div class="col-lg-3">
    		<select class="form-control" name="status" id="status">
    			<option value="" disabled="" selected="">--Pilih Status--</option>
    			<option value="1">Aktiv</option>
    			<option value="2">Tidak Aktiv</option>
    		</select>
    	</div>
    	<div class="col-lg-2">
    		<button type="submit" class="btn btn-primary">Update Status</button>
    	</div>
    </div>
    </form>
	</div>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Tambah Data Supplier</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-member')?>" method="post">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">ID Supplier *</label>
                <input type="number" class="form-control" name="ktp" id="ktp" required="" onchange="ktpmember()">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama*</label>
                <input type="text" class="form-control" name="nama" id="nama" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat"></textarea>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Lokasi</label>
                <input type="text" name="tmp_lahir" id="tmp_lahir" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Join Supplier</label>
                <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">No Telp</label>
                <input type="text" name="telp" id="telp" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" id="email" class="form-control">
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit-member">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Edit Data Supplier</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-update-member')?>" method="post">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">ID Supplier*</label>
                <input type="hidden" name="id_member" id="uid">
                <input type="number" class="form-control" name="ktp" id="uktp" required="" readonly="">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama*</label>
                <input type="text" class="form-control" name="nama" id="unama" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea class="form-control" name="alamat" id="ualamat"></textarea>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Lokasi</label>
                <input type="text" name="tmp_lahir" id="utmp_lahir" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Join Supplier</label>
                <input type="date" name="tgl_lahir" id="utgl_lahir" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">No Telp</label>
                <input type="text" name="telp" id="utelp" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" id="uemail" class="form-control">
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  var url_check_ktp = "<?php echo site_url('check-ktp')?>";
  var url_update_member = "<?php echo site_url('update-member')?>";
</script>

<script src="<?php echo base_url()?>assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/sweetalert/sweet-alerts.js" type="text/javascript"></script>
<?php if ($this->session->flashdata('success_message')): ?>
  <script>
      swal("Success", "<?php echo $this->session->flashdata('success_message')?>", "success");
  </script>
<?php endif; ?>