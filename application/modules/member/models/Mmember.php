<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmember extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function check_ktp($ktp)
	{
		try
		{
			$sql	= "select count(id) as id from member where ktp = ?";
			$query	= $this->db->query($sql,array($ktp))->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function simpan_member($ktp,$nama,$alamat,$tmp_lahir,$tgl_lahir,$telp,$email,$tgl_join,$aktiv,$tgl_input,$pembuat)
	{
		try
		{
			$sql	= "insert into member (ktp,nama,alamat,tempat_lahir,tgl_lahir,telp,email,tgl_join,aktiv,tanggal_input,pembuat) 
					   values (?,?,?,?,?,?,?,?,?,?,?)";
			$query	= $this->db->query($sql,array($ktp,$nama,$alamat,$tmp_lahir,$tgl_lahir,$telp,$email,$tgl_join,$aktiv,
					  $tgl_input,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Member()
	{
		try
		{
			$sql	= "select a.id, a.ktp, a.nama, a.alamat, a.tempat_lahir, a.tgl_lahir, a.telp, a.email, a.aktiv,
					   a.tanggal_input, b.nama_lengkap
					   from member a
					   left join karyawan b
					   on a.pembuat = b.nik
					   order by a.nama";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function hapus_member($member_id)
	{
		try
		{
			$sql	= "delete from member where id = ?";
			$query	= $this->db->query($sql,array($member_id));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_status_member($status,$implode)
	{
		try
		{
			$sql	= "update member set aktiv = ? where id in ('$implode')";
			$query	= $this->db->query($sql,array($status));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_member($member_id)
	{
		try
		{
			$sql	= "select id, ktp, nama, alamat, tempat_lahir, tgl_lahir, telp, email
					   from member where id = ?";
			$query	= $this->db->query($sql,array($member_id))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function simpan_update_member($id_member,$ktp,$nama,$alamat,$tmp_lahir,$tgl_lahir,$telp,$email)
	{
		try
		{
			$sql	= "update member set nama = ?, alamat = ?, tempat_lahir = ?, tgl_lahir = ?, telp = ?, email = ?
					   where id = ?";
			$query	= $this->db->query($sql,array($nama,$alamat,$tmp_lahir,$tgl_lahir,$telp,$email,$id_member));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

}

/* End of file Mmember.php */
/* Location: ./application/modules/member/models/Mmember.php */