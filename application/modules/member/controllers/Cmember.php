<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmember extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mmember');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mmember->getMenu($role_id);
		$data['member']		= $this->Mmember->Member();
		$data['content']	= "Vmember";
		$this->load->view('layout/template',$data);
	}

	public function check_ktp()
	{
		$ktp 			= $this->input->post('param');
		$json['ktp']	= $this->Mmember->check_ktp($ktp);
		echo json_encode($json);
	}

	public function simpan_member()
	{
		$ktp 		= str_replace("'","",$this->input->post('ktp'));
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$tmp_lahir	= str_replace("'","",$this->input->post('tmp_lahir'));
		$tgl_lahir	= date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		$telp 		= $this->input->post('telp');
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$tgl_join	= date('Y-m-d');
		$aktiv		= 1;
		$tgl_input	= date('Y-m-d H:i:s');
		$pembuat	= $this->session->userdata('nik');
		$simpan 	= $this->Mmember->simpan_member($ktp,$nama,$alamat,$tmp_lahir,$tgl_lahir,$telp,$email,$tgl_join,$aktiv,$tgl_input,
					  $pembuat);
		if($simpan == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Member Berhasil Ditambahkan');
			redirect('master-member');
		}
	}

	public function hapus_member($id)
	{
		$member_id	= base64_decode($id);
		$hapus 		= $this->Mmember->hapus_member($member_id);
		if($hapus == TRUE)
		{
			$this->session->set_flashdata('success_message','Hapus Data Berhasil');
			redirect('master-member');
		}
	}

	public function update_status_member()
	{
		$id_member	= $this->input->post('id_member');
		if(empty($id_member))
		{
			echo "Centang member yang akan di update status nya..";
			die();
		}
		else
		{
			$status 	= $this->input->post('status');
			$implode 	= implode("','",$id_member);
			$update 	= $this->Mmember->update_status_member($status,$implode);
			if($update == TRUE)
			{
				$this->session->set_flashdata('success_message','Status Member Berhasil di Update');
				redirect('master-member');
			}
		}
	}

	public function update_member()
	{
		$member_id	= $this->input->post('param');
		$update 	= $this->Mmember->update_member($member_id);
		foreach($update as $data)
		{
			$json['id']		= $data['id'];
			$json['ktp']	= $data['ktp'];
			$json['nama']	= $data['nama'];
			$json['alamat']	= $data['alamat'];
			$json['tempat_lahir']	= $data['tempat_lahir'];
			$json['tgl_lahir']		= $data['tgl_lahir'];
			$json['telp']	= $data['telp'];
			$json['email']	= $data['email'];
		}
		echo json_encode($json);
	}

	public function simpan_update_member()
	{
		$id_member 	= $this->input->post('id_member');
		$ktp 		= str_replace("'","",$this->input->post('ktp'));
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$tmp_lahir	= str_replace("'","",$this->input->post('tmp_lahir'));
		$tgl_lahir	= date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		$telp 		= $this->input->post('telp');
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$update 	= $this->Mmember->simpan_update_member($id_member,$ktp,$nama,$alamat,$tmp_lahir,$tgl_lahir,$telp,$email);
		if($update == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Member Berhasil di Update');
			redirect('master-member');
		}
	}

}

/* End of file Cmember.php */
/* Location: ./application/modules/member/controllers/Cmember.php */