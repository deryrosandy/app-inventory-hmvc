<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckaryawan extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mkaryawan');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mkaryawan->getMenu($role_id);
		$data['karyawan']	= $this->Mkaryawan->Karyawan();
		$data['content']	= "Vkaryawan";
		$this->load->view('layout/template',$data);
	}

	public function hapus_karyawan($id)
	{
		$id_karyawan	= base64_decode($id);
		$hapus 			= $this->Mkaryawan->hapus_karyawan($id_karyawan);
		if($hapus == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Karyawan Berhasil Di Hapus');
			redirect('master-karyawan');
		}
	}

	public function update_status_karyawan()
	{
		$id_karyawan	= $this->input->post('id_karyawan');
		if(empty($id_karyawan))
		{
			echo "Centang karyawan yang akan di update status nya..";
			die();
		}
		else
		{
			$status 	= $this->input->post('status');
			$implode 	= implode("','",$id_karyawan);
			$update 	= $this->Mkaryawan->update_status_karyawan($status,$implode);
			if($update == TRUE)
			{
				$this->session->set_flashdata('success_message','Status Karyawan Berhasil di Update');
				redirect('master-karyawan');
			}
		}
	}

	public function check_nik()
	{
		$nik 		= $this->input->post('param');
		$json['id']	= $this->Mkaryawan->check_nik($nik);
		echo json_encode($json);
	}

	public function simpan_karyawan()
	{
		$nik 		= str_replace("'","",$this->input->post('nik'));
		$nama		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$tmp_lahir	= str_replace("'","",$this->input->post('tmp_lahir'));
		$tgl_lahir 	= date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$telp 		= $this->input->post('telp');
		$jns_kelamin	= $this->input->post('jns_kelamin');
		$akses_sistem 	= $this->input->post('akses_sistem');
		$aktiv 			= 1;
		$tanggal_input	= date('Y-m-d H:i:s');
		$pembuat		= $this->session->userdata('nik');
		$password 		= md5('123'.'123');
		$simpan 		= $this->Mkaryawan->simpan_karyawan($nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,$jns_kelamin,
						  $akses_sistem,$aktiv,$tanggal_input,$pembuat,$password);
		if($simpan == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Karyawan Berhasil Ditambahkan');
			redirect('master-karyawan');
		}
	}

	public function update_karyawan()
	{
		$id_karyawan	= $this->input->post('param');
		$update 		= $this->Mkaryawan->update_karyawan($id_karyawan);
		foreach($update as $data)
		{
			$json['id']		= $data['id'];
			$json['nik']	= $data['nik'];
			$json['nama']	= $data['nama_lengkap'];
			$json['alamat']	= $data['alamat'];
			$json['tempat_lahir']	= $data['tempat_lahir'];
			$json['tgl_lahir']		= $data['tgl_lahir'];
			$json['email']	= $data['email'];
			$json['telp']	= $data['telp'];
			$json['jenis_kelamin']	= $data['jenis_kelamin'];
			$json['akses_system']	= $data['akses_system'];
		}
		echo json_encode($json);	
	}

	public function simpan_update_karyawan()
	{
		$id_karyawan 	= $this->input->post('id_karyawan');
		$nik 		= str_replace("'","",$this->input->post('nik'));
		$nama		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$tmp_lahir	= str_replace("'","",$this->input->post('tmp_lahir'));
		$tgl_lahir 	= date('Y-m-d',strtotime($this->input->post('tgl_lahir')));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$telp 		= $this->input->post('telp');
		$jns_kelamin	= $this->input->post('jns_kelamin');
		$akses_sistem 	= $this->input->post('akses_sistem');
		$update 	= $this->Mkaryawan->simpan_update_karyawan($id_karyawan,$nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,
					  $jns_kelamin,$akses_sistem);
		if($update == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Karyawan Berhasil Di Update');
			redirect('master-karyawan');
		}
	}

}

/* End of file Ckaryawan.php */
/* Location: ./application/modules/karyawan/controllers/Ckaryawan.php */