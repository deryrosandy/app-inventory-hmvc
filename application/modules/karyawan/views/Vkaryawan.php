<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweetalert/sweetalert.css">
<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Data Karyawan</b></h4>
		<div>
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
        <i class="fa fa-fw fa-edit"></i> Tambah Karyawan</button>
		</div>
		<br/>
    <form action="<?php echo site_url('update-status-karyawan')?>" method="post">
		<table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">&#10004;</th>
            <th>Nik</th>
            <th>Nama Lengkap</th>
            <th>Alamat</th>
            <th>Email</th>
            <th style="text-align: center;">Status</th>
            <th>Tanggal Input</th>
            <th>Dibuat Oleh</th>
            <th style="text-align: center;">Aksi</th>
          </tr>
      </thead>
      <tbody>
          <?php $n=1;
          foreach($karyawan as $data){?>
          <tr>
            <td style="text-align: center;"><?php echo $n ?></td>
            <td style="text-align: center;">
              <input type="checkbox" name="id_karyawan[]" id="id_karyawan_<?php echo $n ?>" value="<?php echo $data['id']?>">
            </td>
            <td><?php echo $data['nik']?></td>
            <td><?php echo $data['nama_lengkap']?></td>
            <td><?php echo $data['alamat']?></td>
            <td><?php echo $data['email']?></td>
            <td style="text-align: center;">
              <?php if($data['aktiv'] == 1){?>
                <small class="label label-success">Aktiv</small>
              <?php }else{?>
                <small class="label label-danger">Tidak Aktiv</small>
              <?php } ?>
            </td>
            <td><?php echo $data['tanggal_input']?></td>
            <td><?php echo $data['created_by']?></td>
            <td style="text-align: center;">
              <?php $id = base64_encode($data['id'])?>
              <a href="#" title="Edit Karyawan" data-toggle="modal" data-target="#edit-karyawan" onclick="editkaryawan(<?php echo $n ?>)">
                <i class="fa fa-fw fa-edit"></i>
              </a>
              <a href="<?php echo site_url('hapus-karyawan/'.$id)?>" title="Hapus Karyawan" onclick="return confirm('Yakin di Hapus ?')">
                <i class="fa fa-fw fa-close" style="color: red"></i>
              </a>
            </td>
          </tr>
          <?php $n++; } ?>
      </tbody>
  </table>
  <p style="color: red">Note : Centang sebelum melakukan perubahan status karyawan</p>
  <div class="row">
  	<div class="col-lg-3">
  		<select class="form-control" name="status" id="status">
  			<option value="" disabled="" selected="">--Pilih Status--</option>
  			<option value="1">Aktiv</option>
  			<option value="2">Tidak Aktiv</option>
  		</select>
  	</div>
  	<div class="col-lg-2">
  		<button type="submit" class="btn btn-primary">Update Status</button>
  	</div>
  </div>
  </form>
	</div>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Tambah Data Product</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-karyawan')?>" method="post">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nik *</label>
                <input type="text" class="form-control" name="nik" id="nik" required="" onchange="Nikkaryawan()">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Lengkap *</label>
                <input type="text" class="form-control" name="nama" id="nama" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat"></textarea>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Tempat Lahir</label>
                <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" name="email" id="email" style="text-transform: lowercase;">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Telp</label>
                <input type="number" class="form-control" name="telp" id="telp">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Jenis Kelamin</label>
                <select class="form-control" name="jns_kelamin" id="jns_kelamin">
                  <option value="" disabled="" selected="">--Pilih Jenis Kelamin--</option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Akses System *</label>
                <select class="form-control" name="akses_sistem" id="akses_sistem" required="">
                  <option value="" disabled="" selected="">--Pilih Akses System--</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit-karyawan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Edit Data Karyawan</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-update-karyawan')?>" method="post">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nik *</label>
                <input type="hidden" name="id_karyawan" id="uid">
                <input type="text" class="form-control" name="nik" id="unik" required="" readonly="">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Lengkap *</label>
                <input type="text" class="form-control" name="nama" id="unama" required="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea class="form-control" name="alamat" id="ualamat"></textarea>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Tempat Lahir</label>
                <input type="text" class="form-control" name="tmp_lahir" id="utmp_lahir">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" id="utgl_lahir">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" name="email" id="uemail" style="text-transform: lowercase;">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Telp</label>
                <input type="number" class="form-control" name="telp" id="utelp">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Jenis Kelamin</label>
                <select class="form-control" name="jns_kelamin" id="ujns_kelamin">
                  <option value="" disabled="" selected="">--Pilih Jenis Kelamin--</option>
                  <option value="Laki-laki">Laki-laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Akses System *</label>
                <select class="form-control" name="akses_sistem" id="uakses_sistem" required="">
                  <option value="" disabled="" selected="">--Pilih Akses System--</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  var url_check_nik = "<?php echo site_url('check-nik')?>";
  var url_update_karyawan = "<?php echo site_url('update-karyawan')?>";
</script>
<script src="<?php echo base_url()?>assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/sweetalert/sweet-alerts.js" type="text/javascript"></script>
<?php if ($this->session->flashdata('success_message')): ?>
  <script>
      swal("Success", "<?php echo $this->session->flashdata('success_message')?>", "success");
  </script>
<?php endif; ?>