<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkaryawan extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Karyawan()
	{
		try
		{
			$sql	= "select a.id, a.nik, a.nama_lengkap, a.alamat, a.tempat_lahir, a.tgl_lahir, a.email, a.telp,
					   a.aktiv, a.tanggal_input, b.nama_lengkap as created_by
					   from karyawan a
					   left join karyawan b
					   on a.pembuat = b.nik
					   order by a.nama_lengkap";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function hapus_karyawan($id_karyawan)
	{
		try
		{
			$sql	= "delete from karyawan where id = ?";
			$query	= $this->db->query($sql,array($id_karyawan));

			$getnik	= "select nik from karyawan where id = ?";
			$exec1	= $this->db->query($getnik,array($id_karyawan))->result_array();

			foreach($exec1 as $data)
			{
				$nik = $data['nik'];
			}

			$deleteuser	= "delete from user where nik = ?";
			$exec2		= $this->db->query($deleteuser,array($nik));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec2)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_status_karyawan($status,$implode)
	{
		try
		{
			$sql	= "update karyawan set aktiv = ? where id in ('$implode')";
			$query	= $this->db->query($sql,array($status));

			$getnik	= "select nik from karyawan where id in ('$implode')";
			$exec1	= $this->db->query($getnik)->result_array();

			foreach($exec1 as $data)
			{
				$nik[]	= $data['nik'];
			}

			$implode_nik	= implode("','",$nik);
			$update_user	= "update user set aktiv = ? where nik in ('$implode_nik')";
			$exec2 			= $this->db->query($update_user,array($status));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec2)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function check_nik($nik)
	{
		try
		{
			$sql	= "select count(id) as id from karyawan where nik = ?";
			$query	= $this->db->query($sql,array($nik))->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function simpan_karyawan($nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,$jns_kelamin,
						  $akses_sistem,$aktiv,$tanggal_input,$pembuat,$password)
	{
		try
		{
			$sql	= "insert into karyawan (nik,nama_lengkap,alamat,tempat_lahir,tgl_lahir,email,telp,jenis_kelamin,akses_system,
					   aktiv,tanggal_input,pembuat) values (?,?,?,?,?,?,?,?,?,?,?,?)";
			$query	= $this->db->query($sql,array($nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,$jns_kelamin,
						  $akses_sistem,$aktiv,$tanggal_input,$pembuat));
			if($akses_sistem == 1)
			{
				$insert_user	= "insert into user (nik,password,role_id,aktiv,tanggal_input,pembuat) values (?,?,?,?,?,?)";
				$exec1 			= $this->db->query($insert_user,array($nik,$password,1,$aktiv,$tanggal_input,$pembuat));
				$finish			= 1;
			}
			else
			{
				$finish			= 1;
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($finish == 1)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_karyawan($id_karyawan)
	{
		try
		{
			$sql	= "select id, nik, nama_lengkap, alamat, tempat_lahir, tgl_lahir, email, telp, jenis_kelamin,
					   akses_system
					   from karyawan
					   where id = ?";
			$query	= $this->db->query($sql,array($id_karyawan))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function simpan_update_karyawan($id_karyawan,$nik,$nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,
					  $jns_kelamin,$akses_sistem)
	{
		try
		{
			$sql	= "update karyawan set nama_lengkap = ?, alamat = ?, tempat_lahir = ?, tgl_lahir = ?,
					   email = ?, telp = ?, jenis_kelamin = ?, akses_system = ? where id = ?";
			$query	= $this->db->query($sql,array($nama,$alamat,$tmp_lahir,$tgl_lahir,$email,$telp,
					  $jns_kelamin,$akses_sistem,$id_karyawan));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		
		if($query)
		{
			$this->db->trans_commit();
			return $query;
		}	
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

}

/* End of file Mkaryawan.php */
/* Location: ./application/modules/karyawan/models/Mkaryawan.php */