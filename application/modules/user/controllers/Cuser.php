<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuser extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Muser');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Muser->getMenu($role_id);
		$data['user']		= $this->Muser->User();
		$data['content']	= "Vuser";
		$this->load->view('layout/template',$data);
	}

	public function hapus_user($id)
	{
		$id_user	= base64_decode($id);
		$hapus 		= $this->Muser->hapus_user($id_user);
		if($hapus == TRUE)
		{
			$this->session->set_flashdata('success_message','Data User Berhasil Dihapus');
			redirect('master-user');
		}
	}

	public function update_status_user()
	{
		$id_user	= $this->input->post('id_user');
		if(empty($id_user))
		{
			echo "Centang user yang akan di update status nya..";
			die();
		}
		else
		{
			$status 	= $this->input->post('status');
			$implode 	= implode("','",$id_user);
			$update 	= $this->Muser->update_status_user($status,$implode);
			if($update == TRUE)
			{
				$this->session->set_flashdata('success_message','Status User Berhasil di Update');
				redirect('master-user');
			}
		}
	}

}

/* End of file Cuser.php */
/* Location: ./application/modules/user/controllers/Cuser.php */