<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweetalert/sweetalert.css">
<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Data User</b></h4>
    <form action="<?php echo site_url('update-status-user')?>" method="post">
		<table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">&#10004;</th>
            <th>Username</th>
            <th>Nama Lengkap</th>
            <th style="text-align: center;">Status</th>
            <th>Tanggal Input</th>
            <th>Dibuat Oleh</th>
            <th style="text-align: center;">Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $n=1;
        foreach($user as $data){?>
        <tr>
          <td style="text-align: center;"><?php echo $n ?></td>
          <td style="text-align: center;">
            <?php if($data['nik'] == 'administrator'){?>
            <?php }else{?>
              <input type="checkbox" name="id_user[]" id="id_user_<?php echo $n ?>" value="<?php echo $data['id']?>">
            <?php } ?>
          </td>
          <td><?php echo $data['nik']?></td>
          <td><?php echo $data['nama_lengkap']?></td>
          <td style="text-align: center;">
            <?php if($data['aktiv'] == 1){?>
              <small class="label label-success">Aktiv</small>
            <?php }else{?>
              <small class="label label-danger">Tidak Aktiv</small>
            <?php } ?>
          </td>
          <td><?php echo $data['tanggal_input']?></td>
          <td><?php echo $data['created_by']?></td>
          <td style="text-align: center;">
            <?php if($data['nik'] == 'administrator'){?>
            <?php }else{?>
              <?php $id = base64_decode($data['id']) ?>
              <a href="<?php echo site_url('hapus-user/'.$id)?>" title="Hapus User" onclick="return confirm('Yakin di Hapus ?')">
                <i class="fa fa-fw fa-close" style="color: red"></i>
              </a>
            <?php } ?>
          </td>
        </tr>
        <?php $n++; } ?>
      </tbody>
  </table>
  <p style="color: red">Note : Centang sebelum melakukan perubahan status user</p>
  <div class="row">
  	<div class="col-lg-3">
  		<select class="form-control" name="status" id="status">
  			<option value="" disabled="" selected="">--Pilih Status--</option>
  			<option value="1">Aktiv</option>
  			<option value="2">Tidak Aktiv</option>
  		</select>
  	</div>
  	<div class="col-lg-2">
  		<button type="submit" class="btn btn-primary">Update Status</button>
  	</div>
  </div>
  </form>
	</div>
</div>

<script src="<?php echo base_url()?>assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/sweetalert/sweet-alerts.js" type="text/javascript"></script>
<?php if ($this->session->flashdata('success_message')): ?>
  <script>
      swal("Success", "<?php echo $this->session->flashdata('success_message')?>", "success");
  </script>
<?php endif; ?>