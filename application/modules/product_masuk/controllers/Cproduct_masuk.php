<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cproduct_masuk extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mproduct_masuk');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mproduct_masuk->getMenu($role_id);
		$data['faktur']		= $this->Mproduct_masuk->Fakturmasuk();
		$data['content']	= "Vproduct_masuk";
		$this->load->view('layout/template',$data);	
	}

	public function tambah_faktur_masuk()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mproduct_masuk->getMenu($role_id);
		$data['head']		= $this->Mproduct_masuk->Headoffice();
		$data['stokis']		= $this->Mproduct_masuk->Stokis();
		$data['product']	= $this->Mproduct_masuk->Product();
		$nik 				= $this->session->userdata('nik');
		$data['detail']		= $this->Mproduct_masuk->Detail($nik);
		$data['content']	= "Vtambah_faktur";
		$this->load->view('layout/template',$data);
	}

	public function tambah_detail_masuk()
	{
		$kd_produk	= $this->input->post('param1');
		$qty		= $this->input->post('param2');
		$nik		= $this->session->userdata('nik');
		$tambah 	= $this->Mproduct_masuk->tambah_detail_masuk($kd_produk,$qty,$nik);
		if($tambah == TRUE)
		{
			$json['status']	= 1;
			echo json_encode($json);
		}
	}

	public function hapus_detail_masuk()
	{
		$id_detail	= $this->input->post('param');
		$hapus 		= $this->Mproduct_masuk->hapus_detail_masuk($id_detail);
		if($hapus == TRUE)
		{
			$json['status']	= 1;
			echo json_encode($json);
		}
	}

	public function simpan_tambah_faktur()
	{
		$no_faktur 		= str_replace("'","",$this->input->post('no_faktur'));
		$pengirim		= $this->input->post('pengirim');
		$penerima 		= $this->input->post('penerima');
		$tgl_masuk		= date('Y-m-d',strtotime($this->input->post('tgl_masuk')));
		$kd_produk 		= $this->input->post('kd_produk');
		$qty			= $this->input->post('qty');
		$jml_produk		= count($kd_produk);
		$tanggal_input	= date('Y-m-d H:i:s');
		$pembuat		= $this->session->userdata('nik');
		$master 		= $this->Mproduct_masuk->simpan_master($no_faktur,$pengirim,$penerima,$tgl_masuk,$jml_produk,
						  $tanggal_input,$pembuat);
		if($master == TRUE)
		{
			for($i = 0; $i < $jml_produk; $i++)
			{
				$detail = $this->Mproduct_masuk->simpan_detail($kd_produk[$i],$qty[$i],$tanggal_input,$pembuat);
			}
			if($detail == TRUE)
			{
				$this->session->set_flashdata('success_message','Data Faktur Berhasil Disimpan');
				redirect('transaksi-product-masuk');
			}
		}
	}

	public function print_faktur_masuk($id)
	{
		$faktur_id		= base64_decode($id);
		$data['master']	= $this->Mproduct_masuk->Masterprint($faktur_id);
		$data['detail']	= $this->Mproduct_masuk->Detailprint($faktur_id);
		$this->load->view('Vprint_faktur_masuk',$data);
	}

}

/* End of file Cproduct_masuk.php */
/* Location: ./application/modules/product_masuk/controllers/Cproduct_masuk.php */