<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproduct_masuk extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Headoffice()
	{
		try
		{
			$sql	= "select distinct id, nama, alamat from head_office";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Stokis()
	{
		try
		{
			$sql	= "select distinct id, nama_stokis, alamat from stokis";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Product()
	{
		try
		{
			$sql	= "select id, kd_produk, nama_produk from product where aktiv = 1 order by kd_produk";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Detail($nik)
	{
		try
		{
			$sql	= "select a.id, a.kd_produk, b.nama_produk, a.qty from detail_in a
					   left join product b
					   on a.kd_produk = b.kd_produk
					   where (a.nomor_faktur is null or a.nomor_faktur = '')
					   and a.pembuat = ?";
			$query	= $this->db->query($sql,array($nik))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function tambah_detail_masuk($kd_produk,$qty,$nik)
	{
		try
		{
			$sql	= "select count(id) as id from detail_in
					   where (nomor_faktur is null or nomor_faktur = '')
					   and pembuat = ?
					   and kd_produk = ?";
			$query  = $this->db->query($sql,array($nik,$kd_produk))->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}

			if($id > 0)
			{
				$getqty	= "select qty from detail_in 
						   where (nomor_faktur is null or nomor_faktur = '')
					   	   and pembuat = ?
					       and kd_produk = ?";
				$exec1	= $this->db->query($getqty,array($nik,$kd_produk))->result_array();

				$qtys	= 0;
				foreach($exec1 as $data)
				{
					$qtys	= $data['qty'];
				}

				$fqty		= $qty + $qtys;

				$update 	= "update detail_in set qty = ?
							   where (nomor_faktur is null or nomor_faktur = '')
					   	  	   and pembuat = ?
					           and kd_produk = ?";
				$exec2 		= $this->db->query($update,array($fqty,$nik,$kd_produk));
			}
			else
			{
				$insert 	= "insert into detail_in (kd_produk,qty,pembuat) values (?,?,?)";
				$exec2 		= $this->db->query($insert,array($kd_produk,$qty,$nik));
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec2)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function hapus_detail_masuk($id_detail)
	{
		try
		{
			$sql	= "delete from detail_in where id = ?";
			$query	= $this->db->query($sql,array($id_detail));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_master($no_faktur,$pengirim,$penerima,$tgl_masuk,$jml_produk,
						  $tanggal_input,$pembuat)
	{
		try
		{
			$sql	= "insert into transaksi_in (nomor_faktur,jml_product,pengirim,tgl_masuk,penerima,tanggal_input,pembuat) 
					   values (?,?,?,?,?,?,?)";
			$query	= $this->db->query($sql,array($no_faktur,$jml_produk,$pengirim,$tgl_masuk,$penerima,$tanggal_input,$pembuat));

			$update	= "update detail_in set nomor_faktur = ?
					   where nomor_faktur is null
					   and pembuat = ?";
			$exec 	= $this->db->query($update,array($no_faktur,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_detail($kd_produk,$qty,$tanggal_input,$pembuat)
	{
		try
		{
			$sql	= "select count(id) as id from stock where kd_product = ?";
			$query	= $this->db->query($sql,array($kd_produk))->result_array();

			$id 	= 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}

			if($id > 0)
			{
				$getqty	= "select qty from stock where kd_product = ?";
				$exec 	= $this->db->query($getqty,array($kd_produk))->result_array();

				foreach($exec as $data)
				{
					$qts = $data['qty'];
				}

				$fqty	= $qty + $qts;

				$update	= "update stock set qty = ? where kd_product = ?";
				$exec2	= $this->db->query($update,array($fqty,$kd_produk));
			}
			else
			{
				$insert	= "insert into stock (kd_product,qty) values (?,?)";
				$exec2	= $this->db->query($insert,array($kd_produk,$qty));
			}

			if($exec2)
			{
				$history	= "insert into stock_history (kd_product,qty_in,qty_out,tanggal_input,pembuat) values (?,?,?,?,?)";
				$exec3		= $this->db->query($history,array($kd_produk,$qty,0,$tanggal_input,$pembuat));
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($exec3)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Fakturmasuk()
	{
		try
		{
			$sql	= "select a.id, a.nomor_faktur, a.jml_product, a.tgl_masuk, a.tanggal_input, b.nama_lengkap,
					   c.nama, d.nama_stokis
					   from transaksi_in a
					   left join karyawan b
					   on a.pembuat = b.nik
					   left join head_office c
					   on a.pengirim = c.id
					   left join stokis d
					   on a.penerima = d.id
					   order by a.id";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;		
	}

	public function Masterprint($faktur_id)
	{
		try
		{
			$sql	= "select a.id, a.nomor_faktur, b.nama, c.nama_stokis, a.tgl_masuk, a.tanggal_input,
					   c.alamat, c.telp, c.fax, c.email, b.alamat as head_alamat, b.telp as head_telp,
					   b.email as head_email
					   from transaksi_in a
					   left join head_office b
					   on a.pengirim = b.id
					   left join stokis c
					   on a.penerima = c.id
					   where a.id = ?";
			$query	= $this->db->query($sql,array($faktur_id))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function Detailprint($faktur_id)
	{
		try
		{
			$sql	= "select nomor_faktur from transaksi_in where id = ?";
			$query	= $this->db->query($sql,array($faktur_id))->result_array();

			foreach($query as $data)
			{
				$nomor_faktur = $data['nomor_faktur'];
			}

			$detail = "select a.kd_produk, a.qty, b.nama_produk from detail_in a
					   left join product b
					   on a.kd_produk = b.kd_produk
					   where a.nomor_faktur = ?"; 
			$exec 	= $this->db->query($detail,array($nomor_faktur))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $exec;
	}
}

/* End of file Mproduct_masuk.php */
/* Location: ./application/modules/product_masuk/models/Mproduct_masuk.php */