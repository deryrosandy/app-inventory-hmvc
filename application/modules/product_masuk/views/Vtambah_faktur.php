<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<form action="<?php echo site_url('simpan-tambah-faktur')?>" method="post">
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
            			<label for="exampleInputEmail1">Nomor Faktur *</label>
            			<input type="text" class="form-control" name="no_faktur" id="no_faktur" required="">
            		</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
            			<label for="exampleInputEmail1">Pengirim</label>
            			<?php foreach($head as $data){?>
            			<input type="text" class="form-control" readonly="" value="<?php echo $data['nama']?> [ <?php echo $data['alamat']?> ]">
            			<input type="hidden" class="form-control" name="pengirim" id="pengirim" value="<?php echo $data['id']?>">
            			<?php } ?>
            		</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
            			<label for="exampleInputEmail1">Penerima</label>
            			<?php foreach($stokis as $data){?>
            			<input type="text" class="form-control" readonly="" value="<?php echo $data['nama_stokis']?> [ <?php echo $data['alamat']?> ]">
            			<input type="hidden" class="form-control" name="penerima" id="penerima" value="<?php echo $data['id']?>">
            			<?php } ?>
            		</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
            			<label for="exampleInputEmail1">Tgl Masuk *</label>
            			<input type="date" name="tgl_masuk" id="tgl_masuk" required="" class="form-control">
            		</div>
				</div>
			</div>
			<h4 style="font-family: 'Timew New Rowman'">Detail Product</h4>
			<hr/>
			<div class="row">
				<div class="col-lg-3">
					<select class="form-control" name="produk" id="produk">
						<option value="" disabled="" selected="">--Pilih Product--</option>
						<?php foreach($product as $data){?>
						<option value="<?php echo $data['kd_produk']?>">[ <?php echo $data['kd_produk']?> ] <?php echo $data['nama_produk']?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-lg-2">
					<input type="number" name="qty" id="qty" placeholder="Qty (pcs)" class="form-control">
				</div>
				<div class="col-lg-2">
					<button type="button" class="btn btn-success btn-sm" onclick="Tambahdetail_masuk()">Tambah Product</button>
				</div>
			</div>
			<hr/>
			<p style="color: red;">Note : Input detail product terlebih dahulu, kemudian input master</p>
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="text-align: center;">No</th>
						<th>Nama Product</th>
						<th style="text-align: center;">Qty</th>
						<th style="text-align: center;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $n=1;
					foreach($detail as $data){?>
					<tr>
						<td style="text-align: center;">
							<?php echo $n ?>
							<input type="hidden" name="id_detail" id="id_detail_<?php echo $n ?>" value="<?php echo $data['id']?>">	
						</td>
						<td>
							[ <?php echo $data['kd_produk']?> ] <?php echo $data['nama_produk']?>
							<input type="hidden" name="kd_produk[]" value="<?php echo $data['kd_produk']?>">		
						</td>
						<td style="text-align: center;">
							<?php echo $data['qty'] ?> pcs
							<input type="hidden" name="qty[]" value="<?php echo $data['qty']?>">
						</td>
						<td style="text-align: center;">
							<?php $id = base64_decode($data['id']) ?>
							<a href="#" title="Hapus Produk" onclick="Hapusdetail_masuk(<?php echo $n ?>)">
			                	<i class="fa fa-fw fa-close" style="color: red"></i>
			              	</a>
						</td>
					</tr>
					<?php $n++; } ?> 
				</tbody>
			</table>
			<a href="<?php echo site_url('transaksi-product-masuk')?>">
			<button type="button" class="btn btn-danger btn-sm">Batal</button>
			</a>
			<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
		</form>
	</div>
</div>

<script>
	var url_tambah_detail_masuk	= "<?php echo site_url('tambah-detail-masuk')?>";
	var url_hapus_detail_masuk	= "<?php echo site_url('hapus-detail-masuk')?>";
</script>