<html>
<head>
	<title style="font-family: 'Times New Rowman'; color: #fff">Stookis System</title>
	<style>
		table {
		    border-collapse: collapse; 
		    border:1px solid #69899F;
		} 
		table td{
		    border:1px dotted #000000;
		    padding:2px;
		    font-family: 'Times New Rowman';
		   font-size: 11px;
		}
		table td:first-child{
		    border-left:1px solid #000000;
		}
		table th{
		   border:1px solid #69899F;
		   padding:2px;
		   font-family: 'Times New Rowman';
		   font-size: 12px;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
</head>
	<body style="font-family: 'Times New Rowman'">
		<h3 style="font-family: 'Times New Rowman'; text-align: center;">FAKTUR MASUK</h3>
		<?php foreach($master as $data){?>
		<p style="font-family: 'Times New Rowman'; text-align: center; font-size: 11px">
			<?php echo $data['nama_stokis']?> - <?php echo $data['alamat'] ?> (<?php echo $data['telp']?> / <?php echo $data['email']?>)
		</p>
		<hr/>
		<h4 style="font-family: 'Times New Rowman'; text-align: center;">Nomor Faktur : <?php echo $data['nomor_faktur']?></h4>
		<p style="font-family: 'Times New Rowman'; text-align: center;font-size: 11px">Tanggal Masuk : <?php echo $data['tgl_masuk']?></p>
		<p style="font-family: 'Times New Rowman'; font-size: 12px">Pengirim : <?php echo $data['nama']?> - <?php echo $data['head_alamat']?>
			(<?php echo $data['head_telp']?> / <?php echo $data['head_email']?>)
		</p>
		<p style="font-family: 'Times New Rowman'; font-size: 12px">Penerima : <?php echo $data['nama_stokis']?> - <?php echo $data['alamat']?>
			(<?php echo $data['telp']?> / <?php echo $data['email']?>)</p>
		<?php } ?>
		<p style="font-family: 'Times New Rowman'; font-size: 12px"><b>Detail Product : </b></p>
		<table class="table table-bordered">
 		<thead>
	 		<tr>
	 			<th style="text-align: center;">No</th>
	 			<th>Nama Product</th>
	 			<th style="text-align: center;">Qty (Pcs)</th>
	 		</tr>
	 	</thead>
	 	<tbody>
	 		<?php $n = 1;
	 		foreach($detail as $data){?>
	 		<tr>
	 			<td style="text-align: center;"><?php echo $n ?></td>
	 			<td>[ <?php echo $data['kd_produk']?> ] <?php echo $data['nama_produk']?></td>
	 			<td style="text-align: center;"><?php echo $data['qty']?></td>
	 		</tr>
	 		<?php $n++; } ?>
	 	</tbody>
	 	<script>
			window.print();
		</script>
	</body>
</html>