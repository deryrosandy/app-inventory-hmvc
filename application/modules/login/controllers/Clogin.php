<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clogin extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		$this->load->model('Mlogin');
	}

	public function index()
	{
		$data['title'] 	= 'Sistem Inventori - Login';
		$this->load->view('Vlogin', $data);
	}

	public function verify_login()
	{
		$username 	= $this->input->post('username');
		$password	= md5($this->input->post('password').'123');
		$check		= $this->Mlogin->check_login($username,$password);
		if($check > 0)
		{
			$session_user 	= $this->Mlogin->session_user($username);
			foreach($session_user as $data)
			{
				$this->session->set_userdata('logged',TRUE);
				$this->session->set_userdata('nik',$data['nik']);
				$this->session->set_userdata('nama_lengkap',$data['nama_lengkap']);
				$this->session->set_userdata('alamat',$data['alamat']);
				$this->session->set_userdata('tempat_lahir',$data['tempat_lahir']);
				$this->session->set_userdata('tgl_lahir',$data['tgl_lahir']);
				$this->session->set_userdata('email',$data['email']);
				$this->session->set_userdata('telp',$data['telp']);
				$this->session->set_userdata('jenis_kelamin',$data['jenis_kelamin']);
				$this->session->set_userdata('role_id',$data['role_id']);
			}
			redirect('dashboard');
		}
		else
		{
			$this->session->set_flashdata('erorr_message','Username atau Password Salah..');
			redirect('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}

/* End of file Clogin.php */
/* Location: ./application/modules/login/controllers/Clogin.php */