<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogin extends CI_Model {

	public function check_login($username,$password)
	{
		try
		{
			$sql	= "select count(id) as id from user
					   where nik = ? and password = ?
					   and aktiv = ?";
			$query	= $this->db->query($sql,array($username,$password,1))->result_array();

			$id = 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		return $id;
	}

	public function session_user($username)
	{
		try
		{
			$sql	= "select a.nik, a.nama_lengkap, a.alamat, a.tempat_lahir, a.tgl_lahir, a.email,
					   a.telp, a.jenis_kelamin, b.role_id
					   from karyawan a
					   left join user b
					   on a.nik = b.nik
					   where a.nik = ?";
			$query	= $this->db->query($sql,array($username))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

}

/* End of file Mlogin.php */
/* Location: ./application/modules/login/models/Mlogin.php */