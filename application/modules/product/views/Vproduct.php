<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweetalert/sweetalert.css">
<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Data Product</b></h4>
		<div>
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
        <i class="fa fa-fw fa-edit"></i> Tambah Product</button>
		</div>
		<br/>
    <form action="<?php echo site_url('update-status-product')?>" method="post">
		<table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr>
            <th style="text-align: center;">No</th>
            <th style="text-align: center;">&#10004;</th>
            <th>Kode Product</th>
            <th>Nama Product</th>
            <th style="text-align: center;">Status</th>
            <th>Tanggal Input</th>
            <th>Dibuat Oleh</th>
            <th style="text-align: center;">Aksi</th>
          </tr>
      </thead>
      <tbody>
          <?php $n=1;
          foreach($product as $data){?>
          <tr>
            <td style="text-align: center;"><?php echo $n ?></td>
            <td style="text-align: center;">
              <input type="checkbox" name="id[]" id="id_produk_<?php echo $n ?>" value="<?php echo $data['id']?>">
            </td>
            <td><?php echo $data['kd_produk']?></td>
            <td><?php echo $data['nama_produk']?></td>
            <td style="text-align: center;">
              <?php if($data['aktiv'] == 1){?>
                <small class="label label-success">Aktiv</small>
              <?php }else{?>
                <small class="label label-danger">Tidak Aktiv</small>
              <?php } ?>
            </td>
            <td><?php echo $data['tanggal_input']?></td>
            <td><?php echo $data['nama_lengkap']?></td>
            <td style="text-align: center;">
              <?php $id = base64_encode($data['id'])?>
              <a href="#" title="Edit Produk" data-toggle="modal" data-target="#edit-product" onclick="editproduct(<?php echo $n ?>)">
                <i class="fa fa-fw fa-edit"></i>
              </a>
              <a href="<?php echo site_url('hapus-product/'.$id)?>" title="Hapus Produk" onclick="return confirm('Yakin di Hapus ?')">
                <i class="fa fa-fw fa-close" style="color: red"></i>
              </a>
            </td>
          </tr>
          <?php $n++; } ?>
      </tbody>
  </table>
  <p style="color: red">Note : Centang sebelum melakukan perubahan status product</p>
  <div class="row">
  	<div class="col-lg-3">
  		<select class="form-control" name="status" id="status">
  			<option value="" disabled="" selected="">--Pilih Status--</option>
  			<option value="1">Aktiv</option>
  			<option value="0">Tidak Aktiv</option>
  		</select>
  	</div>
  	<div class="col-lg-2">
  		<button type="submit" class="btn btn-primary">Update Status</button>
  	</div>
  </div>
	</div>
  </form>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Tambah Data Product</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-product')?>" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Kode Product *</label>
            <input type="text" class="form-control" name="kd_product" id="kd_product" style="text-transform: uppercase;" required="" onchange="KodeProduct()">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Product *</label>
            <input type="text" class="form-control" name="nm_product" id="nm_product" required="">
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit-product">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Edit Data Product</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-update-product')?>" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Kode Product *</label>
            <input type="hidden" name="id_product" id="eid_product">
            <input type="text" class="form-control" name="kd_product" id="ekd_product" readonly="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Product *</label>
            <input type="text" class="form-control" name="nm_product" id="enm_product" required="">
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  var url_check_kode_product  = "<?php echo site_url('check-kode-product')?>";
  var url_update_product      = "<?php echo site_url('update-product')?>";
</script>

<script src="<?php echo base_url()?>assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/sweetalert/sweet-alerts.js" type="text/javascript"></script>
<?php if ($this->session->flashdata('success_message')): ?>
  <script>
      swal("Success", "<?php echo $this->session->flashdata('success_message')?>", "success");
  </script>
<?php endif; ?>