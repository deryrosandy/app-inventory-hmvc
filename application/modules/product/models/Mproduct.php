<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproduct extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function check_kode_product($kd_product)
	{
		try
		{
			$sql	= "select count(id) as id from product
					   where kd_produk = ?";
			$query	= $this->db->query($sql,array($kd_product))->result_array();

			$id = 0;
			foreach($query as $data)
			{
				$id = $data['id'];
			}
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $id;
	}

	public function simpan_product($kd_product,$nm_product,$aktiv,$tgl_input,$pembuat)
	{
		try
		{
			$sql	= "insert into product (kd_produk,nama_produk,aktiv,tanggal_input,pembuat) values (?,?,?,?,?)";
			$query	= $this->db->query($sql,array($kd_product,$nm_product,$aktiv,$tgl_input,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Product()
	{
		try
		{
			$sql	= "select a.id, a.kd_produk, a.nama_produk, a.aktiv, a.tanggal_input,
					   b.nama_lengkap
					   from product a
					   left join karyawan b
					   on a.pembuat = b.nik
					   order by a.kd_produk";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function hapus_product($id_produk)
	{
		try
		{
			$sql	= "delete from product where id = ?";
			$query	= $this->db->query($sql,array($id_produk));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_product($id_produk)
	{
		try
		{
			$sql	= "select id, kd_produk, nama_produk from product where id = ?";
			$query	= $this->db->query($sql,array($id_produk))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function simpan_update_product($id_product,$nm_product)
	{
		try
		{
			$sql	= "update product set nama_produk = ? where id = ?";
			$query	= $this->db->query($sql,array($nm_product,$id_product));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_status_product($status,$implode)
	{
		try
		{
			$sql	= "update product set aktiv = ? where id in ('$implode')";
			$query	= $this->db->query($sql,array($status));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

}

/* End of file Mproduct.php */
/* Location: ./application/modules/product/models/Mproduct.php */