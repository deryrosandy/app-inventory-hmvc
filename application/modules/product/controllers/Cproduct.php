<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cproduct extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mproduct');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mproduct->getMenu($role_id);
		$data['product']	= $this->Mproduct->Product();
		$data['content']	= "Vproduct";
		$this->load->view('layout/template',$data);
	}

	public function check_kode_product()
	{
		$kd_product 	= $this->input->post('param');
		$json['id']     = $this->Mproduct->check_kode_product($kd_product);
		echo json_encode($json);
	}

	public function simpan_product()
	{
		$kd_product 	= str_replace("'","",strtoupper($this->input->post('kd_product')));
		$nm_product		= str_replace("'","",$this->input->post('nm_product'));
		$aktiv 			= 1;
		$tgl_input		= date('Y-m-d H:i:s');
		$pembuat 		= $this->session->userdata('nik');
		$simpan 		= $this->Mproduct->simpan_product($kd_product,$nm_product,$aktiv,$tgl_input,$pembuat);
		if($simpan == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Product Berhasil Ditambahkan');
			redirect('master-product');
		}
	}

	public function hapus_product($id)
	{
		$id_produk	= base64_decode($id);
		$hapus 		= $this->Mproduct->hapus_product($id_produk);
		if($hapus == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Product Berhasil di Hapus');
			redirect('master-product');
		}
	}

	public function update_product()
	{
		$id_produk 	= $this->input->post('param');
		$update 	= $this->Mproduct->update_product($id_produk);
		foreach($update as $data)
		{
			$json['id_produk']		= $data['id'];
			$json['kd_produk']		= $data['kd_produk'];
			$json['nama_produk']	= $data['nama_produk'];
		}
		echo json_encode($json);
	}

	public function simpan_update_product()
	{
		$id_product 	= $this->input->post('id_product');
		$kd_product 	= str_replace("'","",strtoupper($this->input->post('kd_product')));
		$nm_product		= str_replace("'","",$this->input->post('nm_product'));
		$update 		= $this->Mproduct->simpan_update_product($id_product,$nm_product);
		if($update == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Product Berhasil di Update');
			redirect('master-product');
		}
	}

	public function update_status_product()
	{
		$id_product	= $this->input->post('id');
		if(empty($id_product))
		{
			echo "Centang product yang akan di update status nya..";
			die();
		}
		else
		{
			$status 	= $this->input->post('status');
			$implode 	= implode("','",$id_product);
			$update 	= $this->Mproduct->update_status_product($status,$implode);
			if($update == TRUE)
			{
				$this->session->set_flashdata('success_message','Status Product Berhasil di Update');
				redirect('master-product');
			}
		}
	}

}

/* End of file Cproduct.php */
/* Location: ./application/modules/product/controllers/Cproduct.php */