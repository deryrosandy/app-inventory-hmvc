<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Laporan Product Keluar</b></h4>
		<br/>
		<form action="<?php echo site_url('print-laporan-keluar')?>" method="post">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
		                <label for="exampleInputEmail1">Tanggal Dari *</label>
		                <input type="date" class="form-control" name="tgl_dari" id="tgl_dari" required="">
		             </div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
		                <label for="exampleInputEmail1">Tanggal Sampai *</label>
		                <input type="date" class="form-control" name="tgl_sampai" id="tgl_sampai" required="">
		            </div>
				</div>
			</div>
			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          	<button type="submit" class="btn btn-primary">Cetak</button>
		</form>
	</div>
</div>