<?php
header("Content-type: application/vnd-ms-excel");
 
$date = date("d/m/Y");
header("Content-Disposition: attachment; filename=Laporan_Product_Keluar".$date.".xls");

?>
<h4>Laporan Product Keluar</h4>
<table id="example1" class="table table-bordered table-striped"border="1">
      <thead>
      	<tr>
      		<th style="text-align: center;">No</th>
      		<th>Nomor Faktur</th>
                  <th>Pengirim</th>
                  <th>Penerima</th>
                  <th>Nama Product</th>
                  <th>Qty (pcs)</th>
                  <th>Tanggal Kirim</th>
                  <th>Tanggal Input</th>
                  <th>Dibuat Oleh</th>
      	</tr>
      </thead>
      <tbody>
      	<?php $n=1;
      	foreach($report as $data){?>
      	<tr>
      		<td style="text-align: center;"><?php echo $n ?></td>
                  <td><?php echo $data['nomor_faktur']?></td>
                  <td><?php echo $data['nama_stokis']?></td>
                  <td><?php echo $data['nama']?></td>
      		<td>[ <?php echo $data['kd_produk']?> ] <?php echo $data['nama_produk']?></td>
                  <td><?php echo $data['qty']?></td>
                  <td><?php echo $data['tgl_kirim']?></td>
      		<td><?php echo $data['tanggal_input']?></td>
                  <td><?php echo $data['nama_lengkap']?></td>
      	</tr>
      	<?php $n++; } ?>
      </tbody>
</table>