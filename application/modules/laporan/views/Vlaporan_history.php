<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Laporan History Stock</b></h4>
		<br/>
		<form action="<?php echo site_url('print-history-stock')?>" method="post">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
		                <label for="exampleInputEmail1">Tanggal Dari *</label>
		                <input type="date" class="form-control" name="tgl_dari" id="tgl_dari" required="">
		             </div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
		                <label for="exampleInputEmail1">Tanggal Sampai *</label>
		                <input type="date" class="form-control" name="tgl_sampai" id="tgl_sampai" required="">
		            </div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
		                <label for="exampleInputEmail1">Product *</label>
		                <select class="form-control" name="product" id="product" required="">
		                	<option value="" disabled="" selected="">--Pilih Product--</option>
		                	<?php foreach($product as $data){?>
		                	<option value="<?php echo $data['kd_produk']?>">[ <?php echo $data['kd_produk']?> ] <?php echo $data['nama_produk']?></option>
		                	<?php } ?>
		                </select>
		            </div>
				</div>
			</div>
			<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          	<button type="submit" class="btn btn-primary">Cetak</button>
		</form>
	</div>
</div>