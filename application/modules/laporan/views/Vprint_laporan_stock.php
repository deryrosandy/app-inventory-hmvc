<?php
header("Content-type: application/vnd-ms-excel");
 
$date = date("d/m/Y");
header("Content-Disposition: attachment; filename=Laporan_Stock".$date.".xls");

?>
<h4>Laporan Stock Product</h4>
<table id="example1" class="table table-bordered table-striped"border="1">
      <thead>
      	<tr>
      		<th style="text-align: center;">No</th>
      		<th>Nama Product</th>
      		<th style="text-align: center;">Qty (pcs)</th>
      	</tr>
      </thead>
      <tbody>
      	<?php $n=1;
      	foreach($stock as $data){?>
      	<tr>
      		<td style="text-align: center;"><?php echo $n ?></td>
      		<td>[ <?php echo $data['kd_product']?> ] <?php echo $data['nama_produk']?></td>
      		<td style="text-align: center;"><?php echo $data['qty']?></td>
      	</tr>
      	<?php $n++; } ?>
      </tbody>
</table>