<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Stock Product</b></h4>
		<div>
			<a href="<?php echo site_url('print-laporan-stock')?>">
				<button type="button" class="btn btn-success">
        		<i class="fa fa-fw fa-print"></i> Cetak Excel</button>
        	</a>
		</div>
		<br/>
		<table id="example1" class="table table-bordered table-striped">
      		<thead>
      			<tr>
      				<th style="text-align: center;">No</th>
      				<th>Nama Product</th>
      				<th style="text-align: center;">Qty (pcs)</th>
      			</tr>
      		</thead>
      		<tbody>
      			<?php $n=1;
      			foreach($stock as $data){?>
      			<tr>
      				<td style="text-align: center;"><?php echo $n ?></td>
      				<td>[ <?php echo $data['kd_product']?> ] <?php echo $data['nama_produk']?></td>
      				<td style="text-align: center;"><?php echo $data['qty']?></td>
      			</tr>
      			<?php $n++; } ?>
      		</tbody>
      	</table>
	</div>
</div>