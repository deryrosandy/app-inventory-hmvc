<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlaporan_history extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Product()
	{
		try
		{
			$sql	= "select id, kd_produk, nama_produk from product where aktiv = 1 order by kd_produk";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function print_history_stock($tgl_dari,$tgl_sampai,$product)
	{
		try
		{
			$sql	= "select a.id, a.kd_product, b.nama_produk, a.qty_in, a.qty_out, substr(a.tanggal_input,1,10) as tanggal_input
					   from stock_history a
					   left join product b
					   on a.kd_product = b.kd_produk
					   where substr(a.tanggal_input,1,10) between
					   '$tgl_dari' and '$tgl_sampai'
					   and a.kd_product = ?
					   order by a.tanggal_input";
			$query	= $this->db->query($sql,array($product))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

}

/* End of file Mlaporan_history.php */
/* Location: ./application/modules/laporan/models/Mlaporan_history.php */