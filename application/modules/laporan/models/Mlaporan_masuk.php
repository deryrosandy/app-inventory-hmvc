<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlaporan_masuk extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function print_laporan_masuk($tgl_dari,$tgl_sampai)
	{
		try
		{
			$sql	= "select a.id, a.nomor_faktur, b.nama, c.nama_stokis, a.tgl_masuk, a.tanggal_input, d.nama_lengkap,
					   e.kd_produk, e.qty, f.nama_produk
					   from transaksi_in a
					   left join head_office b
					   on a.pengirim = b.id
					   left join stokis c
					   on a.penerima = c.id
					   left join karyawan d
					   on a.pembuat = d.nik
					   left join detail_in e
					   on a.nomor_faktur = e.nomor_faktur
					   left join product f
					   on e.kd_produk = f.kd_produk
					   where substr(a.tanggal_input,1,10) between
					   '$tgl_dari' and '$tgl_sampai'";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

}

/* End of file Mlaporan_masuk.php */
/* Location: ./application/modules/laporan/models/Mlaporan_masuk.php */