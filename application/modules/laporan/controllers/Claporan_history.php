<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Claporan_history extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mlaporan_history');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mlaporan_history->getMenu($role_id);
		$data['product']	= $this->Mlaporan_history->Product();
		$data['content']	= "Vlaporan_history";
		$this->load->view('layout/template',$data);
	}

	public function print_history_stock()
	{
		$tgl_dari	= date('Y-m-d',strtotime($this->input->post('tgl_dari')));
		$tgl_sampai	= date('Y-m-d',strtotime($this->input->post('tgl_sampai')));
		$product 	= $this->input->post('product');
		$data['report']	= $this->Mlaporan_history->print_history_stock($tgl_dari,$tgl_sampai,$product);
		$this->load->view('Vprint_history_stock',$data);
	}

}

/* End of file Claporan_history.php */
/* Location: ./application/modules/laporan/controllers/Claporan_history.php */