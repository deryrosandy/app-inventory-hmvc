<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Claporan_stock extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mlaporan_stock');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mlaporan_stock->getMenu($role_id);
		$data['stock']	= $this->Mlaporan_stock->Stock();
		$data['content']	= "Vlaporan_stock";
		$this->load->view('layout/template',$data);
	}

	public function print_laporan_stock()
	{
		$data['stock']	= $this->Mlaporan_stock->Stock();
		$this->load->view('Vprint_laporan_stock',$data);
	}

}

/* End of file Claporan_stock.php */
/* Location: ./application/modules/laporan/controllers/Claporan_stock.php */