<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Claporan_keluar extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mlaporan_keluar');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mlaporan_keluar->getMenu($role_id);
		$data['content']	= "Vlaporan_keluar";
		$this->load->view('layout/template',$data);
	}

	public function print_laporan_keluar()
	{
		$tgl_dari	= date('Y-m-d',strtotime($this->input->post('tgl_dari')));
		$tgl_sampai	= date('Y-m-d',strtotime($this->input->post('tgl_sampai')));
		$data['report']	= $this->Mlaporan_keluar->print_laporan_keluar($tgl_dari,$tgl_sampai);
		$this->load->view('Vprint_laporan_keluar',$data);
	}

}

/* End of file Claporan_masuk.php */
/* Location: ./application/modules/laporan/controllers/Claporan_masuk.php */