<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstokis extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_stokis($nama,$alamat,$telp,$fax,$email,$tanggal_input,$pembuat)
	{
		try
		{
			$sql	= "insert into stokis (nama_stokis,alamat,telp,fax,email,tanggal_input,pembuat) values (?,?,?,?,?,?,?)";
			$query	= $this->db->query($sql,array($nama,$alamat,$telp,$fax,$email,$tanggal_input,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Stokis()
	{
		try
		{
			$sql	= "select a.id, a.nama_stokis as nama, a.alamat, a.telp, a.fax, a.email, a.tanggal_input, b.nama_lengkap
					   from stokis a
					   left join karyawan b
					   on a.pembuat = b.nik";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function hapus_stokis($id_stokis)
	{
		try
		{
			$sql	= "delete from stokis where id = ?";
			$query	= $this->db->query($sql,array($id_stokis));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_stokis($id_stokis)
	{
		try
		{
			$sql	= "select id, nama_stokis as nama, alamat, telp, fax, email from stokis
					   where id = ?";
			$query	= $this->db->query($sql,array($id_stokis))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function simpan_update_stokis($id,$nama,$alamat,$telp,$fax,$email)
	{
		try
		{
			$sql	= "update stokis set nama_stokis = ?, alamat = ?, telp = ?, fax = ?, email = ?
					   where id = ?";
			$query	= $this->db->query($sql,array($nama,$alamat,$telp,$fax,$email,$id));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

}

/* End of file Mstokis.php */
/* Location: ./application/modules/stokis/models/Mstokis.php */