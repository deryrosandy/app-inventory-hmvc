<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cstokis extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mstokis');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mstokis->getMenu($role_id);
		$data['stokis']		= $this->Mstokis->Stokis();
		$data['content']	= "Vstokis";
		$this->load->view('layout/template',$data);
	}

	public function simpan_stokis()
	{
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$telp 		= str_replace("'","",$this->input->post('telp'));
		$fax		= str_replace("'","",$this->input->post('fax'));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$tanggal_input	= date('Y-m-d H:i:s');
		$pembuat 	= $this->session->userdata('nik');
		$simpan 	= $this->Mstokis->simpan_stokis($nama,$alamat,$telp,$fax,$email,$tanggal_input,$pembuat);
		if($simpan == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Stokis Berhasil Ditambahkan');
			redirect('master-stokis');
		}
	}

	public function hapus_stokis($id)
	{
		$id_stokis	= base64_decode($id);
		$hapus 		= $this->Mstokis->hapus_stokis($id_stokis);
		if($hapus == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Stokis Berhasil di Hapus');
			redirect('master-stokis');
		}
	}

	public function update_stokis()
	{
		$id_stokis 	= $this->input->post('param');
		$update 	= $this->Mstokis->update_stokis($id_stokis);
		foreach($update as $data)
		{
			$json['id']		= $data['id'];
			$json['nama']	= $data['nama'];
			$json['alamat']	= $data['alamat'];
			$json['telp']	= $data['telp'];
			$json['email']	= $data['email'];
			$json['fax']	= $data['fax'];
		}
		echo json_encode($json);
	}

	public function simpan_update_stokis()
	{
		$id 		= $this->input->post('id_stokis');
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$telp 		= str_replace("'","",$this->input->post('telp'));
		$fax		= str_replace("'","",$this->input->post('fax'));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$update 	= $this->Mstokis->simpan_update_stokis($id,$nama,$alamat,$telp,$fax,$email);
		if($update == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Stokis Berhasil di Update');
			redirect('master-stokis');
		}
	}

}

/* End of file Cstokis.php */
/* Location: ./application/modules/stokis/controllers/Cstokis.php */