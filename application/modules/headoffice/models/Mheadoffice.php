<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mheadoffice extends CI_Model {

	public function getMenu($role_id)
	{
		try
		{
			$sql	= "select menu_id from role_menu
					   where role_id = ? and aktiv = ?";
			$query	= $this->db->query($sql,array($role_id,1))->result_array();

			$id[] 	= 0;
			foreach($query as $data)
			{
				$id[] = $data['menu_id'];
			}
			
			if($id != "")
			{
				$menu_id = implode("','",$id);
			}
			else
			{
				$menu_id = 0;
			}

			$getmenu 	= "select id, parent_id, label, path, icon, posisi as position
						   from menu
						   where id in ('$menu_id') and aktiv = 1";

			$arrTree 		= $this->db->query($getmenu)->result();
			$arrTreeById 	= [];
			$arrItems 		= [];

			foreach($arrTree as $objItem)
			{
				$arrTreeById[$objItem->id] = $objItem;
				$objItem->arrChilds = [];
			}

			foreach($arrTreeById as $objItem)
			{
				if($objItem->parent_id == 0) $arrItems[] = $objItem;
				if(isset($arrTreeById[$objItem->parent_id])) $arrTreeById[$objItem->parent_id]->arrChilds[] = $objItem;
			}

			return $arrItems;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function simpan_headoffice($nama,$alamat,$telp,$fax,$email,$situs,$tanggal_input,$pembuat)
	{
		try
		{
			$sql	= "insert into head_office (nama,alamat,telp,fax,email,situs,tanggal_input,pembuat) values (?,?,?,?,?,?,?,?)";
			$query	= $this->db->query($sql,array($nama,$alamat,$telp,$fax,$email,$situs,$tanggal_input,$pembuat));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function Headoffice()
	{
		try
		{
			$sql	= "select a.id, a.nama, a.alamat, a.telp, a.fax, a.situs, a.email, a.tanggal_input, b.nama_lengkap
					   from head_office a
					   left join karyawan b
					   on a.pembuat = b.nik";
			$query	= $this->db->query($sql)->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function hapus_headoffice($id_office)
	{
		try
		{
			$sql	= "delete from head_office where id = ?";
			$query	= $this->db->query($sql,array($id_office));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

	public function update_headoffice($id_office)
	{
		try
		{
			$sql	= "select id, nama, alamat, telp, fax, email, situs from head_office
					   where id = ?";
			$query	= $this->db->query($sql,array($id_office))->result_array();
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
		return $query;
	}

	public function simpan_update_headoffice($id,$nama,$alamat,$telp,$fax,$email,$situs)
	{
		try
		{
			$sql	= "update head_office set nama = ?, alamat = ?, telp = ?, fax = ?, email = ?, situs = ?
					   where id = ?";
			$query	= $this->db->query($sql,array($nama,$alamat,$telp,$fax,$email,$situs,$id));
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}

		if($query)
		{
			$this->db->trans_commit();
			return TRUE;
		}
		else
		{
			$this->db->trans_rollback();
			echo $this->db->_error_message();
			die();
		}
	}

}

/* End of file Mheadoffice.php */
/* Location: ./application/modules/headoffice/models/Mheadoffice.php */