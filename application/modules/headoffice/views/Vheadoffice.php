<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweetalert/sweetalert.css">
<script src="<?php echo base_url()?>assets/jquery/jquery.min.js"></script>
<div class="box">
	<div class="box-body">
		<h4 style="font-family: 'Timew New Rowman'"><b>Data Head Office</b></h4>
		<div>
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
        <i class="fa fa-fw fa-edit"></i> Tambah Head Office</button>
		</div>
		<br/>
		<table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr>
            <th style="text-align: center;">No</th>
            <th>Nama Perusahaan</th>
            <th>Alamat</th>
            <th>Telp</th>
            <th>Fax</th>
            <th>Email</th>
            <th>Website</th>
            <th>Tanggal Input</th>
            <th>Dibuat Oleh</th>
            <th style="text-align: center;">Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $n= 1;
        foreach($head as $data){?>
        <tr>
          <td style="text-align: center;">
            <?php echo $n ?>
            <input type="hidden" name="id_office" id="id_office_<?php echo $n ?>" value="<?php echo $data['id']?>">    
          </td>
          <td><?php echo $data['nama']?></td>
          <td><?php echo $data['alamat']?></td>
          <td><?php echo $data['telp']?></td>
          <td><?php echo $data['fax']?></td>
          <td><?php echo $data['email']?></td>
          <td><?php echo $data['situs']?></td>
          <td><?php echo $data['tanggal_input']?></td>
          <td><?php echo $data['nama_lengkap']?></td>
          <td style="text-align: center;">
            <?php $id = base64_encode($data['id'])?>
            <a href="#" title="Edit Head Office" data-toggle="modal" data-target="#edit-headoffice" onclick="editheadoffice(<?php echo $n ?>)">
              <i class="fa fa-fw fa-edit"></i>
            </a>
            <a href="<?php echo site_url('hapus-headoffice/'.$id)?>" title="Hapus Head Office" onclick="return confirm('Yakin di Hapus ?')">
              <i class="fa fa-fw fa-close" style="color: red"></i>
            </a>
          </td>
        </tr>
        <?php $n++; } ?>
      </tbody>
  </table>
	</div>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Tambah Data Head Office</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-headoffice')?>" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Perusahaan *</label>
            <input type="text" class="form-control" name="nama" id="nama" required="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea class="form-control" name="alamat" id="alamat"></textarea>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Telp</label>
                <input type="text" name="telp" id="telp" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Fax</label>
                <input type="text" name="fax" id="fax" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" id="email" class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Situs Website</label>
            <input type="text" class="form-control" name="situs" id="situs">
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit-headoffice">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-family: 'Timew New Rowman'">Edit Data Head Office</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('simpan-update-headoffice')?>" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Perusahaan *</label>
            <input type="hidden" name="id_headoffice" id="uid">
            <input type="text" class="form-control" name="nama" id="unama" required="">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea class="form-control" name="alamat" id="ualamat"></textarea>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Telp</label>
                <input type="text" name="telp" id="utelp" class="form-control">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Fax</label>
                <input type="text" name="fax" id="ufax" class="form-control">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" name="email" id="uemail" class="form-control">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Situs Website</label>
            <input type="text" class="form-control" name="situs" id="usitus">
          </div>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  var url_update_headoffice = "<?php echo site_url('update-headoffice')?>";
</script>

<script src="<?php echo base_url()?>assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/sweetalert/sweet-alerts.js" type="text/javascript"></script>
<?php if ($this->session->flashdata('success_message')): ?>
  <script>
      swal("Success", "<?php echo $this->session->flashdata('success_message')?>", "success");
  </script>
<?php endif; ?>