<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cheadoffice extends MX_Controller {

	function __construct()
	{
		parent:: __construct();
		
		if($this->session->userdata('logged') != TRUE)
		{
			redirect('logout');
		}
		$this->load->model('Mheadoffice');
	}

	public function index()
	{
		$role_id			= $this->session->userdata('role_id');
		$data['menu']		= $this->Mheadoffice->getMenu($role_id);
		$data['head']		= $this->Mheadoffice->Headoffice();
		$data['content']	= "Vheadoffice";
		$this->load->view('layout/template',$data);
	}

	public function simpan_headoffice()
	{
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$telp 		= str_replace("'","",$this->input->post('telp'));
		$fax		= str_replace("'","",$this->input->post('fax'));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$situs 		= str_replace("'","",$this->input->post('situs'));
		$tanggal_input	= date('Y-m-d H:i:s');
		$pembuat 	= $this->session->userdata('nik');
		$simpan 	= $this->Mheadoffice->simpan_headoffice($nama,$alamat,$telp,$fax,$email,$situs,$tanggal_input,$pembuat);
		if($simpan == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Head Office Berhasil Ditambahkan');
			redirect('master-headoffice');
		}
	}

	public function hapus_headoffice($id)
	{
		$id_office	= base64_decode($id);
		$hapus 		= $this->Mheadoffice->hapus_headoffice($id_office);
		if($hapus == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Head Office Berhasil di Hapus');
			redirect('master-headoffice');
		}
	}

	public function update_headoffice()
	{
		$id_office 	= $this->input->post('param');
		$update 	= $this->Mheadoffice->update_headoffice($id_office);
		foreach($update as $data)
		{
			$json['id']		= $data['id'];
			$json['nama']	= $data['nama'];
			$json['alamat']	= $data['alamat'];
			$json['telp']	= $data['telp'];
			$json['email']	= $data['email'];
			$json['situs']	= $data['situs'];
			$json['fax']	= $data['fax'];
		}
		echo json_encode($json);
	}

	public function simpan_update_headoffice()
	{
		$id 		= $this->input->post('id_headoffice');
		$nama 		= str_replace("'","",$this->input->post('nama'));
		$alamat 	= str_replace("'","",$this->input->post('alamat'));
		$telp 		= str_replace("'","",$this->input->post('telp'));
		$fax		= str_replace("'","",$this->input->post('fax'));
		$email 		= str_replace("'","",strtolower($this->input->post('email')));
		$situs 		= str_replace("'","",$this->input->post('situs'));
		$update 	= $this->Mheadoffice->simpan_update_headoffice($id,$nama,$alamat,$telp,$fax,$email,$situs);
		if($update == TRUE)
		{
			$this->session->set_flashdata('success_message','Data Head Office Berhasil di Update');
			redirect('master-headoffice');
		}
	}

}

/* End of file Cheadoffice.php */
/* Location: ./application/modules/headoffice/controllers/Cheadoffice.php */