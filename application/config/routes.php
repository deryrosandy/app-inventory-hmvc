<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'login/Clogin';
$route['login'] 				= 'login/Clogin';
$route['verify_login'] 			= 'login/Clogin/verify_login';
$route['logout'] 				= 'login/Clogin/logout';


// Dashboard
$route['dashboard'] 			= 'dashboard/Cdashboard';


// Product
$route['master-product'] 		= 'product/Cproduct';
$route['check-kode-product'] 	= 'product/Cproduct/check_kode_product';
$route['simpan-product'] 		= 'product/Cproduct/simpan_product';
$route['hapus-product/(:any)']	= 'product/Cproduct/hapus_product/$1';
$route['update-product'] 		= 'product/Cproduct/update_product';
$route['simpan-update-product'] = 'product/Cproduct/simpan_update_product';
$route['update-status-product'] = 'product/Cproduct/update_status_product';

// Member
$route['master-member'] 		= 'member/Cmember';
$route['check-ktp'] 			= 'member/Cmember/check_ktp';
$route['simpan-member'] 		= 'member/Cmember/simpan_member';
$route['hapus-member/(:any)'] 	= 'member/Cmember/hapus_member/$1';
$route['update-status-member'] 	= 'member/Cmember/update_status_member';
$route['update-member'] 		= 'member/Cmember/update_member';
$route['simpan-update-member'] 	= 'member/Cmember/simpan_update_member';

// Karyawan
$route['master-karyawan'] 			= 'karyawan/Ckaryawan';
$route['hapus-karyawan/(:any)'] 	= 'karyawan/Ckaryawan/hapus_karyawan/$1';
$route['update-status-karyawan'] 	= 'karyawan/Ckaryawan/update_status_karyawan';
$route['check-nik'] 				= 'karyawan/Ckaryawan/check_nik';
$route['simpan-karyawan'] 			= 'karyawan/Ckaryawan/simpan_karyawan';
$route['update-karyawan'] 			= 'karyawan/Ckaryawan/update_karyawan';
$route['simpan-update-karyawan'] 	= 'karyawan/Ckaryawan/simpan_update_karyawan';


// User
$route['master-user'] 			= 'user/Cuser';
$route['hapus-user/(:any)'] 	= 'user/Cuser/hapus_user/$1';
$route['update-status-user'] 	= 'user/Cuser/update_status_user';

// Head office
$route['master-headoffice'] 		= 'headoffice/Cheadoffice';
$route['simpan-headoffice'] 		= 'headoffice/Cheadoffice/simpan_headoffice';
$route['hapus-headoffice/(:any)'] 	= 'headoffice/Cheadoffice/hapus_headoffice/$1';
$route['update-headoffice'] 		= 'headoffice/Cheadoffice/update_headoffice';
$route['simpan-update-headoffice'] 	= 'headoffice/Cheadoffice/simpan_update_headoffice';

// Stokis
$route['master-stokis'] 		= 'stokis/Cstokis';
$route['simpan-stokis'] 		= 'stokis/Cstokis/simpan_stokis';
$route['hapus-stokis/(:any)'] 	= 'stokis/Cstokis/hapus_stokis/$1';
$route['update-stokis'] 		= 'stokis/Cstokis/update_stokis';
$route['simpan-update-stokis'] 	= 'stokis/Cstokis/simpan_update_stokis';

// Product Masuk
$route['transaksi-product-masuk']	= 'product_masuk/Cproduct_masuk';
$route['tambah-faktur-masuk']		= 'product_masuk/Cproduct_masuk/tambah_faktur_masuk';
$route['tambah-detail-masuk']		= 'product_masuk/Cproduct_masuk/tambah_detail_masuk';
$route['hapus-detail-masuk']		= 'product_masuk/Cproduct_masuk/hapus_detail_masuk';
$route['simpan-tambah-faktur']		= 'product_masuk/Cproduct_masuk/simpan_tambah_faktur';
$route['print-faktur-masuk/(:any)']	= 'product_masuk/Cproduct_masuk/print_faktur_masuk/$1';

// Product Keluar
$route['transaksi-product-keluar']	= 'product_keluar/Cproduct_keluar';
$route['tambah-faktur-keluar']		= 'product_keluar/Cproduct_keluar/tambah_faktur_keluar';
$route['get-stock']					= 'product_keluar/Cproduct_keluar/get_stock';
$route['tambah-detail-keluar']		= 'product_keluar/Cproduct_keluar/tambah_detail_keluar';
$route['hapus-detail-keluar']		= 'product_keluar/Cproduct_keluar/hapus_detail_keluar';
$route['simpan-faktur-keluar']		= 'product_keluar/Cproduct_keluar/simpan_faktur_keluar';
$route['print-faktur-keluar/(:any)']	= 'product_keluar/Cproduct_keluar/print_faktur_keluar/$1';

// Laporan Stock
$route['laporan-stock-product']		= 'laporan/Claporan_stock';
$route['print-laporan-stock']		= 'laporan/Claporan_stock/print_laporan_stock';

// Laporan History
$route['laporan-history-product']	= 'laporan/Claporan_history';
$route['print-history-stock']		= 'laporan/Claporan_history/print_history_stock';

// Laporan Product Masuk
$route['laporan-product-masuk']		= 'laporan/Claporan_masuk';
$route['print-laporan-masuk']		= 'laporan/Claporan_masuk/print_laporan_masuk';

// Laporan Product Keluar
$route['laporan-product-keluar']	= 'laporan/Claporan_keluar';
$route['print-laporan-keluar']		= 'laporan/Claporan_keluar/print_laporan_keluar';

// Profile
$route['profile']				= 'profile/Cprofile';
$route['simpan-update-profile']	= 'profile/Cprofile/simpan_update_profile';
$route['password']				= 'profile/Cprofile/password';
$route['simpan-update-password']= 'profile/Cprofile/simpan_update_password';

$route['404_override'] 			= '';
$route['translate_uri_dashes'] 	= FALSE;
