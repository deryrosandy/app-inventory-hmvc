function KodeProduct()
{
	var kd_product = $("#kd_product").val();
	$.ajax({
		type:"POST",
		url:url_check_kode_product,
		dataType:"json",
		data:{param: kd_product},
		success:function(data)
		{
			if(data.id > 0)
			{
				alert('Maaf, Kode Product Sudah Digunakan..');
				$("#kd_product").val("");
				$("#kd_product").focus();
			}
		}
	});
}

function editproduct(id)
{
	var id_produk = $("#id_produk_"+id).val();
	$.ajax({
		type:"POST",
		url:url_update_product,
		dataType:"json",
		data:{param: id_produk},
		success:function(data)
		{
			$("#eid_product").val(data.id_produk);
			$("#ekd_product").val(data.kd_produk);
			$("#enm_product").val(data.nama_produk);
		}
	});
}

function ktpmember()
{
	var ktp = $("#ktp").val();
	$.ajax({
		type:"POST",
		url:url_check_ktp,
		dataType:"json",
		data:{param: ktp},
		success:function(data)
		{
			if(data.ktp > 0)
			{
				alert('Maaf, No KTP Sudah Digunakan..');
				$("#ktp").val("");
				$("#ktp").focus();
			}
		}
	});
}

function editmember(id)
{
	var id_member = $("#id_member_"+id).val();
	$.ajax({
		type:"POST",
		url:url_update_member,
		dataType:"json",
		data:{param: id_member},
		success:function(data)
		{
			$("#uid").val(data.id);
			$("#uktp").val(data.ktp);
			$("#unama").val(data.nama);
			$("#ualamat").val(data.alamat);
			$("#utmp_lahir").val(data.tempat_lahir);
			$("#utgl_lahir").val(data.tgl_lahir);
			$("#utelp").val(data.telp);
			$("#uemail").val(data.email);
		}
	});
}

function Nikkaryawan()
{
	var nik = $("#nik").val();
	$.ajax({
		type:"POST",
		url:url_check_nik,
		dataType:"json",
		data:{param: nik},
		success:function(data)
		{
			if(data.id > 0)
			{
				alert('Nik sudah digunakan');
				$("#nik").val("");
				$("#nik").focus();
			}
		}
	});
}

function editkaryawan(id)
{
	var id_karyawan = $("#id_karyawan_"+id).val();
	$.ajax({
		type:"POST",
		url:url_update_karyawan,
		dataType:"json",
		data:{param: id_karyawan},
		success:function(data)
		{
			$("#uid").val(data.id);
			$("#unik").val(data.nik);
			$("#unama").val(data.nama);
			$("#ualamat").val(data.alamat);
			$("#utmp_lahir").val(data.tempat_lahir);
			$("#utgl_lahir").val(data.tgl_lahir);
			$("#uemail").val(data.email);
			$("#utelp").val(data.telp);
			$("#ujns_kelamin").val(data.jenis_kelamin);
			$("#uakses_system").val(data.akses_system);
		}
	});
}

function editheadoffice(id)
{
	var id_office = $("#id_office_"+id).val();
	$.ajax({
		type:"POST",
		url:url_update_headoffice,
		dataType:"json",
		data:{param: id_office},
		success:function(data)
		{
			$("#uid").val(data.id);
			$("#unama").val(data.nama);
			$("#ualamat").val(data.alamat);
			$("#utelp").val(data.telp);
			$("#ufax").val(data.fax);
			$("#uemail").val(data.email);
			$("#usitus").val(data.situs);
		}
	});
}

function editstokis(id)
{
	var id_stokis = $("#id_stokis_"+id).val();
	$.ajax({
		type:"POST",
		url:url_update_stokis,
		dataType:"json",
		data:{param: id_stokis},
		success:function(data)
		{
			$("#uid").val(data.id);
			$("#unama").val(data.nama);
			$("#ualamat").val(data.alamat);
			$("#utelp").val(data.telp);
			$("#ufax").val(data.fax);
			$("#uemail").val(data.email);
		}
	});
}

function Tambahdetail_masuk()
{
	var produk 	= $("#produk").val();
	var qty 	= $("#qty").val();

	$.ajax({
		type:"POST",
		url:url_tambah_detail_masuk,
		dataType:"json",
		data:{param1 : produk, param2 : qty},
		success:function(data)
		{
			if(data.status == 1)
			{
				document.location.reload();
			}
		}
	});
}

function Hapusdetail_masuk(id)
{
	var id_detail = $("#id_detail_"+id).val();
	$.ajax({
		type:"POST",
		url:url_hapus_detail_masuk,
		dataType:"json",
		data:{param : id_detail},
		success:function(data)
		{
			if(data.status == 1)
			{
				document.location.reload();
			}
		}
	});
}

function getstock()
{
	var produk = $("#produk").val();
	$.ajax({
		type:"POST",
		url:url_get_stock,
		dataType:"json",
		data:{param : produk},
		success:function(data)
		{
			var hasil = "Stock: " + data.qty + "pcs";
			$("#stock").val(hasil);
			$("#tstock").val(data.qty);
		}
	});
}

function checkqty()
{
	var stock = $("#tstock").val();
	var qty	= $("#qty").val();
	
	if(qty > stock)
	{
		alert('Qty melebihi stock..');
		$("#qty").val("");
		$("#qty").focus();
	}
}

function Tambahdetail_keluar()
{
	var produk 	= $("#produk").val();
	var qty 	= $("#qty").val();

	$.ajax({
		type:"POST",
		url:url_tambah_detail_keluar,
		dataType:"json",
		data:{param1 : produk, param2 : qty},
		success:function(data)
		{
			if(data.status == 1)
			{
				document.location.reload();
			}
		}
	});
}

function Hapusdetail_keluar(id)
{
	var id_detail = $("#id_detail_"+id).val();
	$.ajax({
		type:"POST",
		url:url_hapus_detail_keluar,
		dataType:"json",
		data:{param : id_detail},
		success:function(data)
		{
			if(data.status == 1)
			{
				document.location.reload();
			}
		}
	});
}